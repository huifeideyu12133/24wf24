package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.LearningCourseModel;
import com.zb.mapper.LearningCourseMapper;
import com.zb.service.LearningCourseService;
import org.springframework.stereotype.Service;

/**
 * @ClassName LearningCourseServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/03/06 11:59
 **/
@Service
		public class LearningCourseServiceImpl extends ServiceImpl<LearningCourseMapper, LearningCourseModel> implements LearningCourseService {

		}
