package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.LearningCourseModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName LearningCourseMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/03/06 11:59
 **/
@Mapper
public interface LearningCourseMapper extends BaseMapper<LearningCourseModel> {

		}
