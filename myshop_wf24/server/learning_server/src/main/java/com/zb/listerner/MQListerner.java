package com.zb.listerner;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zb.config.MQConfig;
import com.zb.entity.LearningCourseModel;
import com.zb.entity.TaskHisModel;
import com.zb.service.LearningCourseService;
import com.zb.service.TaskHisService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

@Component
public class MQListerner {
    @Resource
    LearningCourseService learningCourseService;
    @Resource
    TaskHisService taskHisService;
    @Resource
    RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = MQConfig.addCourseQueue)
    public void addCourse(String json){
        //转成任务对象
        TaskHisModel taskHis = JSON.parseObject(json, TaskHisModel.class);
        //调用方法，添加课程
        this.add(taskHis);
        //learing模块向order模块发消息
        rabbitTemplate.convertAndSend(
                MQConfig.addCourseokExchange,
                MQConfig.addCourseokRouting ,
                json);
        System.out.println("学习模块完成，给订单发送删除任务的消息完成");


    }

    @Transactional
    public void add(TaskHisModel taskHis){
        String requestBody = taskHis.getRequestBody();
        System.out.println(requestBody);
        //转成课程对象
        LearningCourseModel courseModel = JSON.parseObject(requestBody,
                LearningCourseModel.class);
        System.out.println(courseModel);
        //幂等性
        //查询课程表中，有没有此用户，此课程已经购买
        QueryWrapper<LearningCourseModel> queryWrapper=
                new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(LearningCourseModel::getCourseId, courseModel.getCourseId())
                .eq(LearningCourseModel::getUserId,   courseModel.getUserId() );

        LearningCourseModel one = learningCourseService
                .getOne(queryWrapper);
        //没有此记录，可以添加
        if(ObjectUtils.isEmpty(one)){
            learningCourseService.save(courseModel);
        }else{
            //有此记录，更新
            learningCourseService.updateById(courseModel);
        }
        //添加任务 的历史记录
        TaskHisModel his = taskHisService.getById(taskHis.getId());
        if(ObjectUtils.isEmpty(his)){
            taskHisService.save(taskHis);
            System.out.println("添加历史记录");
        }
    }


}
