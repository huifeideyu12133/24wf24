package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.LearningCourseModel;


/**
 * @ClassName LearningCourseService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/03/06 11:59
 **/
public interface LearningCourseService extends IService<LearningCourseModel> {

        }
