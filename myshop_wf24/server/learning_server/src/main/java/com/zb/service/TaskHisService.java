package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TaskHisModel;


/**
 * @ClassName TaskHisService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/03/06 12:00
 **/
public interface TaskHisService extends IService<TaskHisModel> {

        }
