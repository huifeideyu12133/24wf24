package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TaskHisModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TaskHisMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/03/06 12:00
 **/
@Mapper
public interface TaskHisMapper extends BaseMapper<TaskHisModel> {

		}
