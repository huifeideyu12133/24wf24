package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TaskHisModel;
import com.zb.mapper.TaskHisMapper;
import com.zb.service.TaskHisService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TaskHisServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/03/06 12:00
 **/
@Service
		public class TaskHisServiceImpl extends ServiceImpl<TaskHisMapper, TaskHisModel> implements TaskHisService {

		}
