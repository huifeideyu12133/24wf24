package com.zb.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MQConfig {
    //////////////////发送学习模块的消息队列//////////////////////////
    public static final String addCourseExchange="addCourseExchange";
    public static final String addCourseQueue="addCourseQueue";
    public static final String addCourseRouting="addCourseRouting";

    //创建交换机
    @Bean(addCourseExchange)
    public Exchange createaddCourseExchange(){
        return ExchangeBuilder
                .topicExchange(addCourseExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(addCourseQueue)
    public Queue createaddCourseQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        return new Queue(addCourseQueue,
                true,
                false,
                false,
                null);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingAddExchangeQueue(
            @Qualifier(addCourseExchange) Exchange exchange,
            @Qualifier(addCourseQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(addCourseRouting).noargs();

    }



    //////////////////课程添加后发的消息队列//////////////////////////
    public static final String addCourseokExchange="addCourseokExchange";
    public static final String addCourseokQueue="addCourseokQueue";
    public static final String addCourseokRouting="addCourseokRouting";

    //创建交换机
    @Bean(addCourseokExchange)
    public Exchange createaddCourseokExchange(){
        return ExchangeBuilder
                .topicExchange(addCourseokExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(addCourseokQueue)
    public Queue createaddCourseokQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        return new Queue(addCourseokQueue,
                true,
                false,
                false,
                null);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingAddokExchangeQueue(
            @Qualifier(addCourseokExchange) Exchange exchange,
            @Qualifier(addCourseokQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(addCourseokRouting).noargs();

    }



}
