package com.zb.service;

import com.zb.oauth.util.AuthToken;

/**
 * 自定义 认证业务
 */
public interface AuthService {
    public AuthToken login(String username,
                           String password,
                           String clientId,
                           String clientSecret);
}
