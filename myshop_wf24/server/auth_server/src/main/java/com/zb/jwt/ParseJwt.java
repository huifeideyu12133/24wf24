package com.zb.jwt;

import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

public class ParseJwt {
    public static void main(String[] args) {
        String token="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MjEzOTgwNTc1NCwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiJkNDVhZDZlNi04ZWViLTQ0OGMtYjdjYS0wZTI0YzBlYjRjZjkiLCJjbGllbnRfaWQiOiJzaG9wIiwidXNlcm5hbWUiOiIxNzcwMTI2NTI1OCJ9.dxksh9pCyzSUgdtSE6eZS8f3-nR0tfHhrA5uGUYtOxMpZGvI6IVzFyCypchYwnrEx0C0F9ky2sgLrANmTh_n7imB1o1ZdzqeG3EzIizZUclalrHktSHAlpLT-3E2zb_iTinZc41sklveX_t4AoCw5EzSIZ9yTfy7kCcWWQdhbsD3lw5ezJOqIa14GaIn_4yBj2xRuGR70BDJWjBtxjrIC00MjKWcYz9wV6Fh5_UE-h1c76ouQCSzxHQTE5DNZUt9-J5UNcbA4bzSv_6UXvYynIL9ojFg4jkKJZLnq7pSp20x79GpAQvo1lQpf7rqxGjvvUBnd_5H-gZ0dMf1Y-JGMw";
        String publickey="-----BEGIN PUBLIC KEY-----IIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgnhf8AM3SC2AZTd7AxJW10mN1Eh1/kpL3GI+5mxijC49mmc1NY9FtwxL7FqjBLj5vKEg/75aQWd0AqQVSRV2XMroTlMHhUPu3o0OX1Slq3KWt8vp4oJc8mNUCUQmp/JPnYkfrMg/PmHTXB6GF2f3N0Kp6hbAZA8ADyrsfncXkEoSuBuI7zNruZFArob9hfalyhn5YQEwKlGzLkmmalZ4t9TGSiZYADoL+IOOxgQUy8jVEbgidMD5nRCvT8pS7HVoJEbcQUh/eaLHiRJI5dYaQzT7qFmrHclZGPOjNFkS+wUwrym5tIBAtV+PxqZe13//FGJWb8V5pIVsQ/8C8Etd+QIDAQAB-----END PUBLIC KEY-----";
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publickey));
        System.out.println(jwt.getClaims());
    }
}
