package com.zb.service.impl;

import com.zb.oauth.util.AuthToken;
import com.zb.service.AuthService;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {

    //远程调用对象
    @Resource
    RestTemplate restTemplate;

    //注册信息
    @Resource
    DiscoveryClient discoveryClient;

    //redisTemplate
    @Resource
    RedisTemplate redisTemplate;

    @Override
    public AuthToken login(String username,
                           String password,
                           String clientId,
                           String clientSecret) {
        //用代码实现postman 密码生成令牌
        //从注册中心获取远程地址
        List<ServiceInstance> instances = discoveryClient.getInstances("user-auth");
        ServiceInstance serviceInstance = instances.get(0);
        //地址
        String url="http://"+serviceInstance.getHost()+":"+serviceInstance.getPort()+"/oauth/token";
        System.out.println("url:"+url);

        //参数1 用户信息
        LinkedMultiValueMap<String,String> map=new LinkedMultiValueMap<>();
        map.add("grant_type", "password");
        map.add("username", username);
        map.add("password", password);

        //参数2 加密后的请求头
        LinkedMultiValueMap<String,String> header=new LinkedMultiValueMap<>();
        header.add("Authorization",
                this.httpbasic(clientId,clientSecret ));

        //远程调用，返回令牌信息

        ResponseEntity<Map> exchange = restTemplate.exchange(url,
                HttpMethod.POST,
                new HttpEntity<>(map, header),
                Map.class);
        //获取map结果
        Map exchangeBody = exchange.getBody();
        //封装成AuthToken对象
        AuthToken authToken=new AuthToken();
        //长令牌
        authToken.setAccessToken(exchangeBody.get("access_token").toString());
        //刷新令牌
        authToken.setRefreshToken(exchangeBody.get("refresh_token").toString());
        //短令牌
        authToken.setJti(exchangeBody.get("jti").toString());


        //保存令牌到redis中，hash键用短令牌【值:令牌对象；或者长令牌】
        redisTemplate.boundHashOps("token")
                .put(authToken.getJti(), authToken.getAccessToken());
        return authToken;
    }

    private   String httpbasic(String clientId,
                             String clientSecret){
        String str=clientId+":"+clientSecret;
        byte[] encode = Base64Utils.encode(str.getBytes());
        return "Basic "+new String(encode);

    }

    public static void main(String[] args) {
       // System.out.println(httpbasic("myshop","myshop"));


    }
}
