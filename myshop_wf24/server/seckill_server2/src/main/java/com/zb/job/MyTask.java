package com.zb.job;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zb.entity.TbSeckillGoodsModel;
import com.zb.service.TbSeckillGoodsService;
import com.zb.util.DateUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Component
public class MyTask {
    @Resource
    private TbSeckillGoodsService goodsService;

    @Resource
    private RedisTemplate redisTemplate;



    @Scheduled(cron ="0/20 * * * * *")
    public void fromDbToRedis(){
        System.out.println("定时扫描秒杀商品到redis");
        //获取当前时间
        List<Date> list = DateUtil.getDateMenus();
        for (Date startTime : list) {
            System.out.println(startTime);
            //时间段，作为redis商品的键
            try {
                String extName = DateUtil.data2str(startTime, DateUtil.PATTERN_YYYYMMDDHH);
                String strTime = DateUtil.format(startTime, DateUtil.PATTERN_YYYY_MM_DDHHMM);
                String endTime = DateUtil.format(DateUtil.addDateHour(startTime, 2), DateUtil.PATTERN_YYYY_MM_DDHHMM);
//                System.out.println("extName："+extName);
//                System.out.println("strtime："+strTime+";  endTime:"+endTime);
                //查询符合套件的商品
                //三个条件  秒杀开始时间相等，结束时间在开始时间2小时内  库存量不为0
                LambdaQueryWrapper<TbSeckillGoodsModel> queryWrapper=new LambdaQueryWrapper<>();
                queryWrapper.eq(TbSeckillGoodsModel::getStartTime,strTime)
                        .eq(TbSeckillGoodsModel::getEndTime,endTime)
                        .gt(TbSeckillGoodsModel::getStockCount,0);
                //判断--如果添加过不再添加
                Set keys = redisTemplate.boundHashOps("seckillGoods:" + extName).keys();
                //通过redis中商品id的集合
                if(keys!=null&&keys.size()>0){
                    queryWrapper.notIn(TbSeckillGoodsModel::getId,keys);
                }
                //查询
                List<TbSeckillGoodsModel> goodsModels = goodsService.list(queryWrapper);
                for (TbSeckillGoodsModel tbSeckillGoodsModel : goodsModels) {
                    System.out.println(tbSeckillGoodsModel);
                    //把商品放到redis的hash中
                    redisTemplate.boundHashOps("seckillGoods:"+extName).put(tbSeckillGoodsModel.getId().toString(),tbSeckillGoodsModel);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
