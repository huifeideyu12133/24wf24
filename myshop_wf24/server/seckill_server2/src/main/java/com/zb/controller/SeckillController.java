package com.zb.controller;

import com.zb.entity.TbSeckillGoodsModel;
import com.zb.service.TbSeckillGoodsService;
import com.zb.util.DateUtil;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/seckill")
public class SeckillController {
    @Resource
    TbSeckillGoodsService seckillGoodsService;
    //1、展示所有时间
    @RequestMapping("/dateMenu")
    public List<String> dateMenu(){
        //时间集合
        List<String> date=new ArrayList<>();
        //封装的获取时间的方法
        List<Date> dateMenus = DateUtil.getDateMenus();
        //遍历时间
        for (Date dateMenu : dateMenus) {
            //更改时间的格式
            String entName = DateUtil.data2str(dateMenu, DateUtil.PATTERN_YYYYMMDDHH);
            //将更改过的时间信息保存到时间集合
            date.add(entName);
        }
        return date;
    }

    //2、从redis获取当前时间段下的商品信息
    @RequestMapping("/goodsInfo")
    public List<TbSeckillGoodsModel> goodsInfo(String time){
        return seckillGoodsService.seckillGoodsList(time);
    }

    //3、点击抢购，获取商品详情
    @RequestMapping("/ites")
    public TbSeckillGoodsModel ites(String inputTime,String seckillId){
        return seckillGoodsService.seckilllInfo(inputTime,seckillId);
    }

    //创建订单
    @RequestMapping("/create")
    public String createOrder(String inputTime,String seckillId,String userName){
        return seckillGoodsService.createOrder(inputTime,seckillId,userName);
    }
}
