package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbSeckillGoodsModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbSeckillGoodsMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/03/09 09:47
 **/
@Mapper
public interface TbSeckillGoodsMapper extends BaseMapper<TbSeckillGoodsModel> {

		}
