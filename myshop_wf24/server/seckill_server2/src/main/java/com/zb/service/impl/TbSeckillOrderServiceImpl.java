package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbSeckillOrderModel;
import com.zb.mapper.TbSeckillOrderMapper;
import com.zb.service.TbSeckillOrderService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbSeckillOrderServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/03/09 09:47
 **/
@Service
		public class TbSeckillOrderServiceImpl extends ServiceImpl<TbSeckillOrderMapper, TbSeckillOrderModel>implements TbSeckillOrderService {

		}
