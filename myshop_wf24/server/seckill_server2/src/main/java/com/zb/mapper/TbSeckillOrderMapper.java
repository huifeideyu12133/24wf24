package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbSeckillOrderModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbSeckillOrderMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/03/09 09:47
 **/
@Mapper
public interface TbSeckillOrderMapper extends BaseMapper<TbSeckillOrderModel> {

		}
