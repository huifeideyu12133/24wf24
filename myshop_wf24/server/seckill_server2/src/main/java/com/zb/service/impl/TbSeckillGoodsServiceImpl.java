package com.zb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbSeckillGoodsModel;

import com.zb.entity.TbSeckillOrderModel;
import com.zb.mapper.TbSeckillGoodsMapper;
import com.zb.service.TbSeckillGoodsService;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Timer;

/**
 * @ClassName TbSeckillGoodsServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/03/09 09:47
 **/
@Service
public class TbSeckillGoodsServiceImpl extends ServiceImpl<TbSeckillGoodsMapper, TbSeckillGoodsModel>implements TbSeckillGoodsService {
    @Resource
    RedisTemplate redisTemplate;

    //1、从redis获取时间段的商品集合
    @Override
    public List<TbSeckillGoodsModel> seckillGoodsList(String time) {
        return redisTemplate.boundHashOps("seckillGoods:"+time).values();
    }
    //2、获取秒杀商品详情
    @Override
    public TbSeckillGoodsModel seckilllInfo(String time, String id) {
        return (TbSeckillGoodsModel) redisTemplate.boundHashOps("seckillGoods:"+time).get(id);
    }

    //活动商品下单
    @Override
    public synchronized String createOrder(String inputTime, String seckillId, String userName) {

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //调用本类的方法
        //获取本时间段、某商品的信息
        TbSeckillGoodsModel one = this.seckilllInfo(inputTime, seckillId);
        //判断商品信息是否为空或数量为0
        if(ObjectUtils.isEmpty(one)||one.getStockCount()<=0){
            System.out.println("商品无法秒杀，库存不足");
            return "0";
        }else {
            //创建订单对象
            TbSeckillOrderModel orderModel=new TbSeckillOrderModel();
            //雪花算法生成唯一id
            orderModel.setId(IdWorker.getId());
            orderModel.setMoney(one.getPrice());
            orderModel.setUserId(userName);
            orderModel.setCreateTime(new Date());
            //订单状态   0未支付  1已支付
            orderModel.setStatus("0");
            //订单存到redis中
            redisTemplate.boundHashOps("seckillOrder").put(userName,one);
            //redis中的商品库存-1
            one.setStockCount(one.getStockCount()-1);
            if(one.getStockCount()>0){
                redisTemplate.boundHashOps("seckillGoods:"+inputTime).put(seckillId,one);
            }else {
                //redis库存为0时，redis中删除商品，并更新数据库库存为0
                redisTemplate.boundHashOps("seckillGoods:"+inputTime).delete(seckillId);
                //更新到数据库
                UpdateWrapper<TbSeckillGoodsModel> updateWrapper=new UpdateWrapper<>();
                updateWrapper.lambda().set(TbSeckillGoodsModel::getStockCount,0);
                updateWrapper.lambda().eq(TbSeckillGoodsModel::getId,seckillId);
                //调用本类方法修改库存
                this.update(updateWrapper);
                return "1";
            }
        }
        return "1";
    }
}
