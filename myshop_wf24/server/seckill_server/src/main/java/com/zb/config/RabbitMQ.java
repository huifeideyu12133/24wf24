package com.zb.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.amqp.core.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitMQ {

    public static final String oneseckillExchange="seckillEoneseckillExchangexchange";
    public static final String oneseckillQueue="oneseckillQueue";
    public static final String oneseckillRouting="oneseckillRouting";

    public static final String twoseckillExchange="twoseckillExchange";
    public static final String twoseckillQueue="twoseckillQueue";
    public static final String twoseckillRouting="twoseckillRouting";

    //创建交换机
    @Bean(oneseckillExchange)
    public Exchange createSeckillExchange(){
        return ExchangeBuilder.topicExchange(oneseckillExchange).durable(true).build();
    }

    //创建队列
    @Bean(oneseckillQueue)
    public Queue createSeckillQueue(){
        Map<String,Object> params=new HashMap<>();
        params.put("x-dead-letter-exchange", twoseckillExchange);
        params.put("x-dead-letter-routing-key",twoseckillRouting );
        return new Queue(oneseckillQueue,true,false,false,params);
    }

    //关联交换机
    @Bean
    public Binding bindingSeckill(@Qualifier(oneseckillExchange) Exchange seckillExchange,
                                  @Qualifier(oneseckillQueue)Queue seckillQueue){
        return BindingBuilder.bind(seckillQueue)
                .to(seckillExchange).with(oneseckillRouting).noargs();

    }

    @Bean(twoseckillExchange)
    public Exchange createTwoSeckillExchange(){
        return ExchangeBuilder.topicExchange(twoseckillExchange).durable(true).build();
    }
    //创建队列
    @Bean(twoseckillQueue)
    public Queue createTwoSeckillQueue(){
        return new Queue(twoseckillQueue,true,false,false,null);
    }

    //关联交换机
    @Bean
    public Binding bindingTwoSeckill(@Qualifier(twoseckillExchange) Exchange seckillExchange,
                                  @Qualifier(twoseckillQueue)Queue seckillQueue){
        return BindingBuilder.bind(seckillQueue)
                .to(seckillExchange).with(twoseckillRouting).noargs();

    }

}
