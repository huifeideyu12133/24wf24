package com.zb.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 分布式锁
 */
@Component
public class LockUtil {
    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 上锁
     * @param key 键值 商品编号
     * @return
     */
    public boolean lock(String key){
        RedisConnectionFactory connectionFactory = redisTemplate.getConnectionFactory();
        RedisConnection connection = connectionFactory.getConnection();
        byte[] keyBytes=key.getBytes();
        byte[] valueBytes = "lock".getBytes();
        Boolean aBoolean = connection.setNX(keyBytes, valueBytes);
        if(aBoolean){
            //防止死锁，设置有效期
            connection.expire(keyBytes,60);//60秒
        }
        return aBoolean;
    }

    /**
     * 解锁（删除）
     * @param key
     */
    public void unlock(String key){
        redisTemplate.delete(key);
    }
}
