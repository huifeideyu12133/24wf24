package com.zb;

import com.zb.util.LockUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableDiscoveryClient
//定时任务
@EnableScheduling
//多线程
@EnableAsync
public class Main {
    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext run = SpringApplication.run(Main.class, args);
//        LockUtil bean = run.getBean(LockUtil.class);
//        //加锁
//        boolean lock = bean.lock("lock:1");
//        System.out.println(lock);
//        //停20秒
//        Thread.sleep(20000);
//        //释放锁
//        bean.unlock("lock:1");
    }
}