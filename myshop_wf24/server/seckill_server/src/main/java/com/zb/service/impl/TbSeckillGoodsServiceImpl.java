package com.zb.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbSeckillGoodsModel;
import com.zb.entity.TbSeckillOrderModel;
import com.zb.job.MultiThreadingCreateOrder;
import com.zb.mapper.TbSeckillGoodsMapper;
import com.zb.service.TbSeckillGoodsService;
import com.zb.service.TbSeckillOrderService;
import com.zb.util.SeckillStatus;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @ClassName TbSeckillGoodsServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/03/09 09:47
 **/
@Service
public class TbSeckillGoodsServiceImpl extends ServiceImpl<TbSeckillGoodsMapper, TbSeckillGoodsModel>implements TbSeckillGoodsService {

	@Resource
	RedisTemplate redisTemplate;
	@Override
	public List<TbSeckillGoodsModel> seckillGoodsList(String time) {
		// 时间段的键以 "seckillGoods:" 开头
		return redisTemplate.boundHashOps("seckillGoods:" + time).values();
	}

	@Override
	public TbSeckillGoodsModel seckilllInfo(String time, String id) {
		TbSeckillGoodsModel o = (TbSeckillGoodsModel) redisTemplate.boundHashOps("seckillGoods:" + time).get(id);
		return o;
	}
	@Resource
	MultiThreadingCreateOrder multiThreadingCreateOrder;

	@Override
	public synchronized String create(String time, String id, String username) {
		System.out.println("1 下单业务开始");
		//防止用户重复提交订单  increment自增
		Long increment = redisTemplate.boundHashOps("userCount").increment(username + ":" + id, +1);
		if(increment>1){
			System.out.println("重复抢购");
			return "2";//返回控制器
		}

		//封装一个状态对象
		SeckillStatus seckillStatus=new SeckillStatus(username,new Date(),1,Long.parseLong(id),time);
		//状态对象存到redis中，保证秒杀的公平性
		redisTemplate.boundListOps("userQueue").leftPush(seckillStatus);
		//用户订单状态信息
		redisTemplate.boundHashOps("seckillStatus").put(username,seckillStatus);
		//调用异步方法
		multiThreadingCreateOrder.asyncAddOrder();
		System.out.println("4 下单业务结束");
		return "1";
	}

	@Override
	public SeckillStatus getStatus(String username) {
		return (SeckillStatus) redisTemplate.boundHashOps("seckillStatus").get(username);
	}
}
