package com.zb.job;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zb.util.DateUtil;
import com.zb.entity.TbSeckillGoodsModel;
import com.zb.service.TbSeckillGoodsService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Component
public class MyTask {

    @Resource
    private TbSeckillGoodsService tbSeckillGoodsService;
    @Resource
    private RedisTemplate redisTemplate;

    @Scheduled(cron = "0/20 * * * * *")
    public void fromDbToRedis(){
        System.out.println("定时扫描秒杀商品信息");
        //查询时间端
        List<Date> list = DateUtil.getDateMenus();
        for (Date date : list) {
            System.out.println(date);
            try {
                //时间段作为商品的键
                String extName = DateUtil.data2str(date, DateUtil.PATTERN_YYYYMMDDHH);
                String strtime = DateUtil.format(date, DateUtil.PATTERN_YYYY_MM_DDHHMM);
                String endTime = DateUtil.format(DateUtil.addDateHour(date, 2), DateUtil.PATTERN_YYYY_MM_DDHHMM);
                System.out.println("extName："+extName);
                System.out.println("strtime："+strtime+";  endTime:"+endTime);


                //查询符合条件的商品
                //三个条件 秒杀开始时间相等，结束时间在2小时内，库存量不为0
                LambdaQueryWrapper<TbSeckillGoodsModel> queryWrapper=new LambdaQueryWrapper<>();
                queryWrapper.eq(TbSeckillGoodsModel::getStartTime,strtime)
                .eq(TbSeckillGoodsModel::getEndTime,endTime)
                .gt(TbSeckillGoodsModel::getStockCount,0);

                //防止重复查询redis已经存在的记录
                //获取当前redis中秒杀商品id的集合
                Set keys=redisTemplate.boundHashOps("sekillGoods:"+extName).keys();
                //判断
                if(keys!=null&&keys.size()>0){
                    queryWrapper.notIn(TbSeckillGoodsModel::getId,keys);
                }

                List<TbSeckillGoodsModel> tbSeckillGoodsList = tbSeckillGoodsService.list(queryWrapper);
                for (TbSeckillGoodsModel tbSeckillGoods : tbSeckillGoodsList) {
                    //把商品放到redis中的hash
                    redisTemplate.boundHashOps("seckillGoods:"+extName).put(tbSeckillGoods.getId().toString(),tbSeckillGoods);

                    //获取商品的库存
                    Integer count = tbSeckillGoods.getStockCount();
                    //掉用方法 获取数组
                    Long[] ids = this.getIds(count, tbSeckillGoods.getId());
                    //存入到redis中 商品库存数量的队列
                    redisTemplate.boundListOps("goodsNumQueue"+ tbSeckillGoods.getId())
                            .leftPushAll(ids);
                }



            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Long[] getIds(int len,long id){
        Long[] ids=new Long[len];
        for (int i = 0; i < len; i++) {
            ids[i]=id;
        }
        return ids;
    }
}
