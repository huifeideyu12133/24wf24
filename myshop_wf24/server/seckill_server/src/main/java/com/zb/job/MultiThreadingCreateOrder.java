package com.zb.job;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zb.config.RabbitMQ;
import com.zb.entity.TbSeckillGoodsModel;
import com.zb.entity.TbSeckillOrderModel;
import com.zb.service.TbSeckillGoodsService;
import com.zb.util.LockUtil;
import com.zb.util.SeckillStatus;
import org.apache.ibatis.annotations.Arg;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Date;

@Component
public class MultiThreadingCreateOrder {
    @Resource
    RedisTemplate redisTemplate;

    @Resource
    TbSeckillGoodsService tbSeckillGoodsService;

    @Resource
    LockUtil lockUtil;

    @Resource
    RedissonClient redissonClient;

    @Resource
    RabbitTemplate rabbitTemplate;


    //异步请求
    @Async
    public void asyncAddOrder(){

        //三个参数
        System.out.println("从redis队列获取抢购的状态信息");
        SeckillStatus seckillStatus = (SeckillStatus) redisTemplate.boundListOps("userQueue").rightPop();
        String id=seckillStatus.getGoodsId().toString();//秒杀的商品id
        String time = seckillStatus.getTime(); //秒杀的时间段
        String username = seckillStatus.getUsername();//秒杀的用户


        //获取用户秒杀状态
        SeckillStatus status = (SeckillStatus) redisTemplate.boundHashOps("seckillStatus").get(username);


        //同步代码块
       // synchronized (MultiThreadingCreateOrder.class){

        //获取商品的库存队列
        Object o = redisTemplate.boundListOps("goodsNumQueue:" + id).rightPop();
        if (ObjectUtils.isEmpty(o)){
            System.out.println("库存不足");
            //更改秒杀状态
            status.setStatus(4);//秒杀失败
            redisTemplate.boundHashOps("seckillStatus").put(username, status);
            return;
        }
            System.out.println("2、异步下单开始");
        //        try {
        //            //获得锁，
        //            while (!lockUtil.lock("lock:"+id)){
        //                Thread.sleep(500);
        //            }

//            try {
//                //模拟每一个线程进来停0.5s
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            //分布式--公平锁: 先来的线程先获取锁
            RLock fairLock = redissonClient.getFairLock(id);
            fairLock.lock();//锁，如果加锁成功后超时时间30秒，而且开启了看门狗线程；当快过期时，还剩10秒时业务为完成，将会延长锁时间



            //调用本类方法、获取本时间段，某商品id的商品对象
            TbSeckillGoodsModel one = tbSeckillGoodsService.seckilllInfo(time, id);
//            if (ObjectUtils.isEmpty(one)||one.getStockCount()<=0){
//                System.out.println("商品数量不足，无法下单");
//                //更改秒杀状态
//                status.setStatus(4);//秒杀失败
//                redisTemplate.boundHashOps("seckillStatus").put(username,status);
//                return;
//            }
            //创建订单对象
            TbSeckillOrderModel orderModel=new TbSeckillOrderModel();
            orderModel.setId(IdWorker.getId());
            orderModel.setMoney(one.getPrice());
            orderModel.setUserId(id);
            orderModel.setCreateTime(new Date());
            orderModel.setStatus("0");//未支付
            //储存订单信息到redis
            redisTemplate.boundHashOps("seckillOrder").put(username,orderModel);
            //商品库存-1
            one.setStockCount(one.getStockCount()-1);
            System.out.println(one.getStockCount());
            //保存到redis中,方便后面库存判断
            if (one.getStockCount()>0){
                redisTemplate.boundHashOps("seckillGoods:" + time).put(id,one);
            }else
                //只有库存到0了，商品在redis中删除,此商品数据库库存更新为0
                if (one.getStockCount()==0){
                    System.out.println("删除");
                    redisTemplate.boundHashOps("seckillGoods:" + time).delete(id);
                    UpdateWrapper<TbSeckillGoodsModel> updateWrapper=new UpdateWrapper<>();
                    updateWrapper.lambda().eq(TbSeckillGoodsModel::getId,id)
                            .set(TbSeckillGoodsModel::getStockCount,0);
                    tbSeckillGoodsService.update(updateWrapper);
                }
            status.setStatus(2);
            redisTemplate.boundHashOps("seckillStatus").put(username,status);
            System.out.println("3、异步抢单结束");

            //发送mq消息 消息数据  设置有效期属性
            rabbitTemplate.convertAndSend(RabbitMQ.oneseckillExchange,
                    RabbitMQ.oneseckillRouting,JSON.toJSONString(seckillStatus),
                    new MessagePostProcessor() {
                        @Override
                        public Message postProcessMessage(Message message) throws AmqpException {
                            message.getMessageProperties().setExpiration("20000");//20s测试
                            return message;
                        }
                    });






            //释放锁
            fairLock.unlock();

            //        } catch (Exception ex) {
            //            ex.printStackTrace();
            //        }finally {
            //            //释放锁
            //            lockUtil.unlock("lock:"+id);
            //        }
//      }
    }
}
