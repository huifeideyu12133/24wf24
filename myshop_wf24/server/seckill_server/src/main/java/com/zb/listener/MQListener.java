package com.zb.listener;

import com.alibaba.fastjson.JSON;
import com.zb.config.RabbitMQ;
import com.zb.entity.TbSeckillGoodsModel;
import com.zb.service.TbSeckillGoodsService;
import com.zb.util.SeckillStatus;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

@Component
public class MQListener {
    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Resource
    private TbSeckillGoodsService goodsService;

    @RabbitListener(queues = RabbitMQ.twoseckillQueue)
    public void rollbackData(String json){
        //mq的数据
        SeckillStatus seckillStatus= JSON.parseObject(json, SeckillStatus.class);
        //redis中获取
        String time = seckillStatus.getTime();
        String id = seckillStatus.getGoodsId().toString();
        TbSeckillGoodsModel goodsModel =(TbSeckillGoodsModel) redisTemplate.boundHashOps("seckillGoods:" + time).get(id);
        //判断是否为空
        if(ObjectUtils.isEmpty(goodsModel)){
            //redis为空，没有此商品，秒杀完了也会为0
            //从数据库获取商品
            TbSeckillGoodsModel one = goodsService.getById(id);
           //修改数据库的库存
            one.setStockCount(1);
            //更新到数据库
            goodsService.updateById(one);
            //Redis商品库存
            redisTemplate.boundHashOps("seckillGoods:"+time).put(id,one);
        }else {
            //redis中存在此商品，只需要修改商品库存量
            System.out.println("修改redis中的库存量。。。");
            goodsModel.setStockCount(goodsModel.getStockCount()+1);
            //覆盖到redis中
            redisTemplate.boundHashOps("seckillGoods:"+time).put(id.toString(),goodsModel);
        }
        //添加商品库存队列
        redisTemplate.boundListOps("goodsNumQueue:"+id).leftPush(id);
        //修改订单
        TbSeckillGoodsModel orderModel = (TbSeckillGoodsModel) redisTemplate.boundHashOps("seckillOrder").get(seckillStatus.getUsername());
        orderModel.setStatus("4");//失效订单
        //抢购状态改为支付超时
        SeckillStatus status =(SeckillStatus) redisTemplate.boundHashOps("seckillStatus").get(seckillStatus.getUsername());
        status.setStatus(3);//超时未支付
        //修改redis的订单抢购状态
        redisTemplate.boundHashOps("seckillStatus").put(seckillStatus.getUsername(),status);
        //删除用户点击记录
        redisTemplate.boundHashOps("userCount").delete(seckillStatus.getUsername()+":"+id);

        //监听支付成功后模块发送的消息

        //1、取用户名
        //在redis seckillOrder获取订单，订单状态修改为以支付1
        //操作数据库，订单入口
        //redis中订单可以删除

        //2、抢购状态seckillStatus 改为5 支付完成，保存在redis中

    }

}
