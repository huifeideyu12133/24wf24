package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbSeckillGoodsModel;
import com.zb.util.SeckillStatus;

import java.util.List;


/**
 * @ClassName TbSeckillGoodsService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/03/09 09:47
 **/
public interface TbSeckillGoodsService extends IService<TbSeckillGoodsModel> {
        //1、从redis获取时间段的商品集合
        public List<TbSeckillGoodsModel> seckillGoodsList(String time);

        //2、获取秒杀商品详情
        TbSeckillGoodsModel seckilllInfo(String time,String id);

        public String create(String time, String id, String username);

        //返回订单状态
        public SeckillStatus getStatus(String username);

}
