package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbSeckillOrderModel;


/**
 * @ClassName TbSeckillOrderService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/03/09 09:47
 **/
public interface TbSeckillOrderService extends IService<TbSeckillOrderModel> {

}
