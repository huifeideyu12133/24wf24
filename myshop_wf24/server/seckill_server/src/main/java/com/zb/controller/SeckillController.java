package com.zb.controller;

import com.zb.util.DateUtil;
import com.zb.entity.TbSeckillGoodsModel;
import com.zb.service.TbSeckillGoodsService;
import com.zb.util.SeckillStatus;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

@RestController
@RequestMapping("/seckill")
public class SeckillController {

    @Resource
    TbSeckillGoodsService tbSeckillGoodsService;
    @Resource
    RedisTemplate redisTemplate;

    //1，获取并显示时间段
    @RequestMapping("/dateMenu")
    public List<String> dateMenu() {
        List<String> date=new ArrayList<>();
        List<Date> dateMenus = DateUtil.getDateMenus();
        for (Date dateMenu : dateMenus) {
            String extName = DateUtil.data2str(dateMenu, DateUtil.PATTERN_YYYYMMDDHH);
            date.add(extName);
        }
        return date;

        //流方法

    }

    //2,从redis获取该时间段下所有的商品的信息
    @RequestMapping("/goodsInfo")
    public List<TbSeckillGoodsModel> goodsInfo(String time) {
        List<TbSeckillGoodsModel> goodsModelList = tbSeckillGoodsService.seckillGoodsList(time);
        return goodsModelList;
    }

    //3,点击抢购，进入到商品详情页面
    @RequestMapping("/ites")
    public TbSeckillGoodsModel ites(String seckillId, String inputTime) {
        return tbSeckillGoodsService.seckilllInfo(inputTime,seckillId);
    }


    //下单
    @RequestMapping("/create")
    public  String createOrder(String time,String id,String username){
        return tbSeckillGoodsService.create(time,id,username);
    }

    //查询抢单状态的
    @RequestMapping("getStatus")
    public SeckillStatus getStatus(String username) {
        return  (SeckillStatus) redisTemplate.boundHashOps("seckillStatus").get(username);
    }
}
