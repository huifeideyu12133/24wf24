package com.zb.util;

import com.zb.model.FileModel;
import org.csource.common.MyException;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;

@Component
public class FileUtil {
    static{
        try {
            //静态块，启动读取配置文件
            String path = new ClassPathResource("fdfs_client.conf").getPath();
            ClientGlobal.init(path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }

    }

    //文件上传
    public  String[] testUpdate(FileModel fileModel) throws Exception {
        TrackerClient trackerClient=new TrackerClient();
        TrackerServer connection=trackerClient.getConnection();
        StorageClient storageClient=new StorageClient(connection,null);
        //上传
        String[] path = storageClient
                .upload_file(fileModel.getContent(),
                        fileModel.getExt(),
                        null);
        //控制台输出地址
        for (String s : path) {
            System.out.println(s);
        }
        return path;

    }

    //文件删除
    public  static void delete() throws Exception {
        TrackerClient trackerClient=new TrackerClient();
        TrackerServer connection=trackerClient.getConnection();
        StorageClient storageClient=new StorageClient(connection,null);
        //删除
        int group1 = storageClient.delete_file("group1",
                "M00/00/00/wKjIgGWhJKeAegyQAoNGo6eQBPQ811.mp4");
        //删除成功返回0
        System.out.println(group1);
    }


    //返回服务器地址
    public  String fileUrl() throws IOException {
        TrackerClient trackerClient=new TrackerClient();
        TrackerServer connection=trackerClient.getConnection();
        String url="http://"
                +connection.getInetSocketAddress()
                .getHostString()
                +":"+ClientGlobal.getG_tracker_http_port();
        return url;


    }

    //下载
    public static void downLoad() throws Exception {
        TrackerClient trackerClient=new TrackerClient();
        TrackerServer connection=trackerClient.getConnection();
        StorageClient storageClient=new StorageClient(connection,null);
        //下载
        byte[] bytes = storageClient
                .download_file("group1",
                        "M00/00/00/wKjIgGWhJQ-APkE7AABNjfjynSE482.png");
        FileOutputStream fos=
                new FileOutputStream("h://abc.png");
        fos.write(bytes);
        fos.close();
        System.out.println("下载成功");

    }




    public static void main(String[] args) throws Exception {
        //testUpdate();
        //delete();
        //System.out.println(fileUrl());
        downLoad();
    }

}
