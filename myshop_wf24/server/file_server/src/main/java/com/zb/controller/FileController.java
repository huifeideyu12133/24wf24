package com.zb.controller;

import com.zb.model.FileModel;
import com.zb.util.FileUtil;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
public class FileController {

    @Resource
    private FileUtil fileUtil;
    @PostMapping("/myupload")
    public String myupload(@RequestParam("file") MultipartFile file) throws  Exception{
        //创建文件对象
        FileModel fileModel=new FileModel(
                file.getOriginalFilename(),
                file.getBytes(),
                StringUtils.getFilenameExtension(
                        file.getOriginalFilename()),
                "xm");
        //调用工具类的上传方法
        String[] path = fileUtil.testUpdate(fileModel);
        return fileUtil.fileUrl()+"/"+path[0]+"/"+path[1];

    }
}
