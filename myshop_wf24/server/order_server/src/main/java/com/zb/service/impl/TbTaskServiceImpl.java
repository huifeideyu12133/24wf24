package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbTaskModel;
import com.zb.mapper.TbTaskMapper;
import com.zb.service.TbTaskService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName TbTaskServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/03/06 10:00
 **/
@Service
public class TbTaskServiceImpl extends ServiceImpl<TbTaskMapper, TbTaskModel> implements TbTaskService {

    @Override
    public List<TbTaskModel> findBeforList() {
        return baseMapper.findBeforList();
    }

    @Override
    public Integer lock(Long id, Integer version) {
        return baseMapper.lock(id, version);
    }
}
