package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbTaskHisModel;
import com.zb.mapper.TbTaskHisMapper;
import com.zb.service.TbTaskHisService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbTaskHisServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/03/06 10:00
 **/
@Service
		public class TbTaskHisServiceImpl extends ServiceImpl<TbTaskHisMapper, TbTaskHisModel> implements TbTaskHisService {

		}
