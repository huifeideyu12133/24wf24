package com.zb.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Configuration
public class FeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        //发给order服务请求头的信息
        ServletRequestAttributes requestAttributes =
                (ServletRequestAttributes) RequestContextHolder
                        .getRequestAttributes();
        System.out.println("请求头属性============"+requestAttributes);
        if(requestAttributes!=null){
            HttpServletRequest request = requestAttributes.getRequest();
            //请求头的信息 key value结构
            Enumeration<String> headerNames = request.getHeaderNames();
            while(headerNames.hasMoreElements()){
                String key = headerNames.nextElement();
                String value = request.getHeader(key);
                System.out.println(key+"=========="+value);
                //在请求模板中将令牌的数据添加到请求头中
                requestTemplate.header(key,value);
            }


        }
    }
}
