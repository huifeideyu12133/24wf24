package com.zb.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.client.SkuFeignClient;
import com.zb.config.MQConfig;
import com.zb.entity.TbOrderItemModel;
import com.zb.entity.TbOrderModel;
import com.zb.mapper.TbOrderItemMapper;
import com.zb.mapper.TbOrderMapper;
import com.zb.service.CartService;
import com.zb.service.TbOrderService;
import com.zb.util.IdWorker;
//import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName TbOrderServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/02/19 11:24
 **/
@Service
public class TbOrderServiceImpl extends ServiceImpl<TbOrderMapper, TbOrderModel> implements TbOrderService {

    @Resource
    TbOrderItemMapper tbOrderItemMapper;

    @Resource
    CartService cartService;

    @Resource
    private SkuFeignClient skuFeignClient;



    @Resource
    private RabbitTemplate rabbitTemplate;


   // @GlobalTransactional
    @Override
    public Integer addOrder(TbOrderModel order, String[] skuIds) {
        //设置编号
        order.setId(IdWorker.getId());
        Integer sumNum=0;//订单的总数量
        Integer sumMoney=0;//订单的所有商品的总金额
        //调用购物车业务中查询选中购物车商品集合
        List<TbOrderItemModel> list = cartService.search(order.getUsername(), skuIds);
        //遍历商品明细，添加到商品明细表
        for (TbOrderItemModel orderItemModel : list) {
            sumMoney=sumMoney+orderItemModel.getMoney();
            sumNum=sumNum+ orderItemModel.getNum();
            //订单明细的orderId属性
            orderItemModel.setOrderId(order.getId());
            //添加订单明细到数据库
            tbOrderItemMapper.insert(orderItemModel);
            //远程调用商品更改库存服务
            skuFeignClient.updateNum(
                    orderItemModel.getSkuId(),
                    orderItemModel.getNum());
        }
        //设置订单属性
        order.setTotalMoney(sumMoney);
        order.setTotalNum(sumNum);
        order.setBuyerRate("0");        //0:未评价，1：已评价
        order.setSourceType("1");       //来源，1：WEB
        order.setOrderStatus("0");      //0:未完成,1:已完成，2：已退货
        order.setPayStatus("0");        //0:未支付，1：已支付，2：支付失败
        order.setConsignStatus("0");    //0:未发货，1：已发货，2：已收货
        //添加订单到数据库
        int n = baseMapper.insert(order);
        //如果订单添加成功，从redis删除购物车数据
        if(n>0){
            cartService.delete(order.getUsername(), skuIds);

            //下单成功后，发送消息
            //订单数据 json字符串
            String orderJson= JSON.toJSONString(order);

            rabbitTemplate.convertAndSend(MQConfig.oneExchange,
                    MQConfig.oneRouting,
                    orderJson, new MessagePostProcessor() {
                        @Override
                        public Message postProcessMessage(Message message) throws AmqpException {
                            //属性
                            message.getMessageProperties()
                                    .setExpiration("15000");
                            return message;
                        }
                    });


        }
        return n;
    }
}
