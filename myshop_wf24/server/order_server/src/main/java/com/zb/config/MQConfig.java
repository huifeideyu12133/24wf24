package com.zb.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

//mq的配置类
@Configuration
public class MQConfig {
    public static final String payExchange="payExchange";
    public static final String payQueue="payQueue";
    public static final String payRouting="payRouting";

    //创建交换机
    @Bean(payExchange)
    public Exchange createPayExchange(){
        return ExchangeBuilder
                .topicExchange(payExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(payQueue)
    public Queue createPayQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        return new Queue(payQueue,
                true,
                false,
                false,
                null);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingPayExchangeQueue(
            @Qualifier(payExchange) Exchange exchange,
            @Qualifier(payQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(payRouting).noargs();

    }

    /////////////////////////////////////////////////
    //订单的延迟队列[死信队列的其中一种方式]
    public static final String oneExchange="oneExchange";
    public static final String oneQueue="oneQueue";
    public static final String oneRouting="oneRouting";

    public static final String twoExchange="twoExchange";
    public static final String twoQueue="twoQueue";
    public static final String twoRouting="twoRouting";

    //创建交换机
    @Bean(oneExchange)
    public Exchange createoneExchange(){
        return ExchangeBuilder
                .topicExchange(oneExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(oneQueue)
    public Queue createoneQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        //死信队列
        Map<String,Object> params=new HashMap<>();
        params.put("x-dead-letter-exchange", twoExchange);
        params.put("x-dead-letter-routing-key",twoRouting );
        return new Queue(oneQueue,
                true,
                false,
                false,
                params);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingoneExchangeQueue(
            @Qualifier(oneExchange) Exchange exchange,
            @Qualifier(oneQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(oneRouting).noargs();

    }

    //创建交换机
    @Bean(twoExchange)
    public Exchange createtwoExchange(){
        return ExchangeBuilder
                .topicExchange(twoExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(twoQueue)
    public Queue createtwoQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        return new Queue(twoQueue,
                true,
                false,
                false,
                null);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingtwoExchangeQueue(
            @Qualifier(twoExchange) Exchange exchange,
            @Qualifier(twoQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(twoRouting).noargs();

    }


    //////////////////发送学习模块的消息队列//////////////////////////
    public static final String addCourseExchange="addCourseExchange";
    public static final String addCourseQueue="addCourseQueue";
    public static final String addCourseRouting="addCourseRouting";

    //创建交换机
    @Bean(addCourseExchange)
    public Exchange createaddCourseExchange(){
        return ExchangeBuilder
                .topicExchange(addCourseExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(addCourseQueue)
    public Queue createaddCourseQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        return new Queue(addCourseQueue,
                true,
                false,
                false,
                null);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingAddExchangeQueue(
            @Qualifier(addCourseExchange) Exchange exchange,
            @Qualifier(addCourseQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(addCourseRouting).noargs();

    }

    //////////////////课程添加后发的消息队列//////////////////////////
    public static final String addCourseokExchange="addCourseokExchange";
    public static final String addCourseokQueue="addCourseokQueue";
    public static final String addCourseokRouting="addCourseokRouting";

    //创建交换机
    @Bean(addCourseokExchange)
    public Exchange createaddCourseokExchange(){
        return ExchangeBuilder
                .topicExchange(addCourseokExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(addCourseokQueue)
    public Queue createaddCourseokQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        return new Queue(addCourseokQueue,
                true,
                false,
                false,
                null);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingAddokExchangeQueue(
            @Qualifier(addCourseokExchange) Exchange exchange,
            @Qualifier(addCourseokQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(addCourseokRouting).noargs();

    }


}
