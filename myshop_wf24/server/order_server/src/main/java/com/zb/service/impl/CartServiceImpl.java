package com.zb.service.impl;

import com.zb.client.SkuFeignClient;
import com.zb.entity.TbOrderItemModel;
import com.zb.entity.TbOrderModel;
import com.zb.service.CartService;
import com.zb.service.TbOrderItemService;
import com.zb.util.IdWorker;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService {
    @Resource
    private SkuFeignClient skuFeignClient;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Override
    public int addCart(String skuId, Integer num, String username) {
        //通过openfeign，获取商品信息
        Map<String, Object> info = skuFeignClient.info(skuId);
        Map<String,Object> sku =(Map<String,Object>)info.get("sku");
        Map<String,Object> spu =(Map<String,Object>)info.get("spu");
        //购物车对象[结构]
        TbOrderItemModel orderItemModel=new TbOrderItemModel();
        orderItemModel.setId(IdWorker.getId());//调用雪花算法得到唯一id
        orderItemModel.setCategoryId1(Integer.parseInt(spu.get("category1Id").toString()));
        orderItemModel.setCategoryId2(Integer.parseInt(spu.get("category2Id").toString()));
        orderItemModel.setCategoryId3(Integer.parseInt(spu.get("category3Id").toString()));
        orderItemModel.setSpuId(spu.get("id").toString());
        orderItemModel.setSkuId(skuId);
        orderItemModel.setName(sku.get("name").toString());
        orderItemModel.setPrice(Integer.parseInt(sku.get("price").toString()));
        orderItemModel.setNum(num);
        orderItemModel.setMoney(orderItemModel.getPrice()*num);//此商品总价
        orderItemModel.setImage(sku.get("image").toString());
        orderItemModel.setWeight(Integer.parseInt(sku.get("weight").toString()));
        //添加到redis中
        redisTemplate.boundHashOps("cart:"+username).put(skuId,orderItemModel );

        return 1;
    }

    @Override
    public List<TbOrderItemModel> getCart(String username) {
        List values = redisTemplate.boundHashOps("cart:" + username).values();
        return values;
    }

    //删除
    public void delete(String username,String[] skuIds){
         redisTemplate.boundHashOps("cart:" + username)
                 .delete(skuIds);
    }

    //获取你选择的商品
    @Override
    public List<TbOrderItemModel> search(
            String username,
            String[] skuIds) {
        List<TbOrderItemModel> list=new ArrayList<>();
        //遍历传过来的skuId的数组
        for (String skuId : skuIds) {
            //从redis获取
            TbOrderItemModel item= (TbOrderItemModel)redisTemplate.boundHashOps("cart:" + username).get(skuId);
            list.add(item);
        }
        return list;
    }
    //下订单

}
