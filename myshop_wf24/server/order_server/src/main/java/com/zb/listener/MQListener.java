package com.zb.listener;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rabbitmq.client.Channel;
import com.zb.config.MQConfig;
import com.zb.entity.TbOrderItemModel;
import com.zb.entity.TbOrderModel;
import com.zb.entity.TbTaskHisModel;
import com.zb.entity.TbTaskModel;
import com.zb.service.TbOrderItemService;
import com.zb.service.TbOrderService;
import com.zb.service.TbTaskHisService;
import com.zb.service.TbTaskService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class MQListener {
    @Resource
    private TbOrderService tbOrderService;
    @Resource
    private TbOrderItemService tbOrderItemService;


    //监听方法
    @RabbitListener(queues = MQConfig.payQueue)
    public void revice(Channel channel,
                       Message message,
                       String json){
        System.out.println("监听到了mq的数据");
        //{"transaction_id":"4200002137202402297746025186","nonce_str":"6c45fa31a9f849d98bdc59c2c90483c6","bank_type":"OTHERS","openid":"oHkLxt8APDoSZJZfrkT6TivhdULY","sign":"796437A4AAC47D30F1A02E482F833EFF","fee_type":"CNY","mch_id":"11473623","cash_fee":"1","out_trade_no":"1278061488505884678","appid":"wxab8acb865bb1637e","total_fee":"1","trade_type":"NATIVE","result_code":"SUCCESS","time_end":"20240229095013","is_subscribe":"N","return_code":"SUCCESS"}
        System.out.println(json);
        //获取订单编号
        //转成map
        Map<String,String> data = JSON.parseObject(json, Map.class);
        String out_trade_no = data.get("out_trade_no");
        //本地业务 获取订单对象
        LambdaQueryWrapper<TbOrderModel> queryWrapper=
                new LambdaQueryWrapper<>();
        queryWrapper.eq(TbOrderModel::getId,out_trade_no);
        TbOrderModel tbOrder = tbOrderService.getOne(queryWrapper);
        //修改订单支付状态
        tbOrder.setPayStatus("1");
        //本地业务 修改订单状态
        tbOrderService.saveOrUpdate(tbOrder);
        System.out.println("修改订单状态成功");

        //订单支付成功后，添加任务
        this.addTask(out_trade_no);


    }
    //添加任务
    public void  addTask(String out_trade_no){
        //获取此订单信息
        TbOrderModel tbOrder = tbOrderService.getById(out_trade_no);
        //获取用户名
        String username = tbOrder.getUsername();
        //获取订单明细信息
        QueryWrapper<TbOrderItemModel> queryWrapper=
                new QueryWrapper<>();
        queryWrapper.lambda().eq(TbOrderItemModel::getOrderId,out_trade_no );
        List<TbOrderItemModel> items = tbOrderItemService.list(queryWrapper);
        for (TbOrderItemModel item : items) {
            System.out.println("-----------"+item);
            //课程数据
            Map<String,Object> requestBody=new HashMap<>();
            requestBody.put("course_id", item.getSkuId());
            requestBody.put("user_id", username.charAt(0));
            requestBody.put("price",item.getPrice() );
            requestBody.put("startTime", "2024-01-01 00:00:00");
            requestBody.put("endTime", "2024-01-01 00:00:00");
            //创建task对象
            TbTaskModel taskModel=new TbTaskModel();
            taskModel.setTaskType("1");
            taskModel.setRequestBody(JSON.toJSONString(requestBody));
            taskModel.setMqExchange("addCourseExchange");
            taskModel.setMqRoutingkey("addCourseRouting");
            taskModel.setCreateTime(new Date());
            taskModel.setUpdateTime(new Date());
            taskModel.setDeleteTime(new Date());
            taskModel.setStatus("0");
            taskModel.setErrormsg("0");
            taskModel.setVersion(1);
            //添加任务
            tbTaskService.save(taskModel);
        }
        System.out.println("添加任务成功");

    }

    //监听，死信的第二个队列
    @RabbitListener(queues = MQConfig.twoQueue)
    public void checkOrder(String json){
        //把监听到数据转成对象
        TbOrderModel tbOrder = JSON.parseObject(json, TbOrderModel.class);
        //获取订单的支付状态
        LambdaQueryWrapper<TbOrderModel> queryWrapper=
                new LambdaQueryWrapper<>();
        queryWrapper.eq(TbOrderModel::getId,tbOrder.getId() );
        TbOrderModel order = tbOrderService.getOne(queryWrapper);
        //没有支付状态没有支付，设为失效,更新到数据
        if(order.getPayStatus().equals("0")){
            order.setOrderStatus("4");//订单状态设置失效
            tbOrderService.saveOrUpdate(order);
            System.out.println("订单状态失效修改成功");
        }


    }
    //注入业务对象

    @Resource
    private TbTaskService tbTaskService;
    @Resource
    private TbTaskHisService tbTaskHisService;


    //购课任务完成后，监听任务的备份
    @RabbitListener(queues = MQConfig.addCourseokQueue)
    public  void backTaskHis(String json){
        //转成任务历史对象
        TbTaskHisModel taskHisModel = JSON.parseObject(
                json, TbTaskHisModel.class);
        //查询要删除的任务
        TbTaskModel taskModel = tbTaskService.getById(
                taskHisModel.getId());
        if(!ObjectUtils.isEmpty(taskModel)){
            //删除任务
            tbTaskService.removeById(taskModel.getId());
            //添加历史记录
            tbTaskHisService.save(taskHisModel);
        }
        System.out.println("已完成的任务清除，备份历史任务完成");


    }




}
