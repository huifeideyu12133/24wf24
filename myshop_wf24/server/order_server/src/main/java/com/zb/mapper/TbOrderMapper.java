package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbOrderModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbOrderMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/02/19 11:24
 **/
@Mapper
public interface TbOrderMapper extends BaseMapper<TbOrderModel> {

		}
