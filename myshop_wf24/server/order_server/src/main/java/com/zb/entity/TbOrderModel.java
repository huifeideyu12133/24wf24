package com.zb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName TbOrderModel
 * @Description 模型对象
 * @Author xm
 * @Date 2024/02/19 11:24
 **/
@Data
    @EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_order")
@ApiModel(value = "TbOrderModel", description = "")
public class TbOrderModel implements Serializable{

private static final long serialVersionUID=1L;

        @ApiModelProperty(value = "订单id")
                    @TableId(value = "id", type = IdType.UUID)
                private String id;

        @ApiModelProperty(value = "数量合计")
        private Integer totalNum;

        @ApiModelProperty(value = "金额合计")
        private Integer totalMoney;

        @ApiModelProperty(value = "优惠金额")
        private Integer preMoney;

        @ApiModelProperty(value = "邮费")
        private Integer postFee;

        @ApiModelProperty(value = "实付金额")
        private Integer payMoney;

        @ApiModelProperty(value = "支付类型，1、在线支付、0 货到付款")
        private String payType;

        @ApiModelProperty(value = "订单创建时间")
        private Date createTime;

        @ApiModelProperty(value = "订单更新时间")
        private Date updateTime;

        @ApiModelProperty(value = "付款时间")
        private Date payTime;

        @ApiModelProperty(value = "发货时间")
        private Date consignTime;

        @ApiModelProperty(value = "交易完成时间")
        private Date endTime;

        @ApiModelProperty(value = "交易关闭时间")
        private Date closeTime;

        @ApiModelProperty(value = "物流名称")
        private String shippingName;

        @ApiModelProperty(value = "物流单号")
        private String shippingCode;

        @ApiModelProperty(value = "用户名称")
        private String username;

        @ApiModelProperty(value = "买家留言")
        private String buyerMessage;

        @ApiModelProperty(value = "是否评价")
        private String buyerRate;

        @ApiModelProperty(value = "收货人")
        private String receiverContact;

        @ApiModelProperty(value = "收货人手机")
        private String receiverMobile;

        @ApiModelProperty(value = "收货人地址")
        private String receiverAddress;

        @ApiModelProperty(value = "订单来源：1:web，2：app，3：微信公众号，4：微信小程序  5 H5手机页面")
        private String sourceType;

        @ApiModelProperty(value = "交易流水号")
        private String transactionId;

        @ApiModelProperty(value = "订单状态 ")
        private String orderStatus;

        @ApiModelProperty(value = "支付状态 0:未支付 1:已支付")
        private String payStatus;

        @ApiModelProperty(value = "发货状态 0:未发货 1:已发货 2:已送达")
        private String consignStatus;

        @ApiModelProperty(value = "是否删除")
        private String isDelete;


        }
