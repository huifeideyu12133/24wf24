package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbTaskHisModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbTaskHisMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/03/06 10:00
 **/
@Mapper
public interface TbTaskHisMapper extends BaseMapper<TbTaskHisModel> {

		}
