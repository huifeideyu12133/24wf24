package com.zb.mapper;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @ClassName TbOrderItemModel
 * @Description 模型对象
 * @Author xm
 * @Date 2024/02/19 11:25
 **/
@Data
    @EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_order_item")
@ApiModel(value = "TbOrderItemModel", description = "")
public class TbOrderItemModel implements Serializable{

private static final long serialVersionUID=1L;

        @ApiModelProperty(value = "ID")
                    @TableId(value = "id", type = IdType.UUID)
                private String id;

        @ApiModelProperty(value = "1级分类")
        private Integer categoryId1;

        @ApiModelProperty(value = "2级分类")
        private Integer categoryId2;

        @ApiModelProperty(value = "3级分类")
        private Integer categoryId3;

        @ApiModelProperty(value = "SPU_ID")
        private String spuId;

        @ApiModelProperty(value = "SKU_ID")
        private String skuId;

        @ApiModelProperty(value = "订单ID")
        private String orderId;

        @ApiModelProperty(value = "商品名称")
        private String name;

        @ApiModelProperty(value = "单价")
        private Integer price;

        @ApiModelProperty(value = "数量")
        private Integer num;

        @ApiModelProperty(value = "总金额")
        private Integer money;

        @ApiModelProperty(value = "实付金额")
        private Integer payMoney;

        @ApiModelProperty(value = "图片地址")
        private String image;

        @ApiModelProperty(value = "重量")
        private Integer weight;

        @ApiModelProperty(value = "运费")
        private Integer postFee;

        @ApiModelProperty(value = "是否退货")
        private String isReturn;


        }
