package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbOrderItemModel;
import com.zb.mapper.TbOrderItemMapper;
import com.zb.service.TbOrderItemService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbOrderItemServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/02/19 11:25
 **/
@Service
		public class TbOrderItemServiceImpl extends ServiceImpl<TbOrderItemMapper, TbOrderItemModel> implements TbOrderItemService {

		}
