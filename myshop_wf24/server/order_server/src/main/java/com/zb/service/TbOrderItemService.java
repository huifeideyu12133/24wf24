package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbOrderItemModel;


/**
 * @ClassName TbOrderItemService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/02/19 11:25
 **/
public interface TbOrderItemService extends IService<TbOrderItemModel> {

        }
