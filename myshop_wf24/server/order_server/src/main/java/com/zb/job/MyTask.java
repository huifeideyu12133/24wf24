package com.zb.job;

import com.alibaba.fastjson.JSON;
import com.zb.config.MQConfig;
import com.zb.entity.TbTaskModel;
import com.zb.service.TbTaskService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class MyTask {
    //注入业务对象
    @Resource
    private TbTaskService tbTaskService;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Scheduled(cron = "0/10 * * * * *")
    public void test(){
        System.out.println("执行定时任务...");
        List<TbTaskModel> list = tbTaskService.findBeforList();
        for (TbTaskModel tbTaskModel : list) {
            System.out.println(tbTaskModel);
            //更改时间设置为当前时间
            TbTaskModel byId = tbTaskService.getById(tbTaskModel.getId());
            byId.setUpdateTime(new Date());
            tbTaskService.updateById(byId);
            //加锁，基于数据库的乐观锁
            if(tbTaskService.lock(byId.getId(),byId.getVersion() )>0) {
                //发送mq
                rabbitTemplate.convertAndSend(
                        MQConfig.addCourseExchange,
                        MQConfig.addCourseRouting,
                        JSON.toJSONString(tbTaskModel));
            }


        }

    }
}
