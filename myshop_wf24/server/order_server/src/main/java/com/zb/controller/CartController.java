package com.zb.controller;

import com.zb.config.TokenDecode;
import com.zb.dto.OrderItemDto;
import com.zb.entity.TbOrderItemModel;
import com.zb.service.CartService;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.naming.Name;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Resource
    private CartService cartService;

    @RequestMapping("/add/{skuId}/{num}")
    public Integer addCart(@PathVariable("skuId") String skuId,
                           @PathVariable("num") Integer num){
        //用户名，后面从token中获取
        //String username="张三";
        String username = TokenDecode.getUserInfo().get("username");
        //调用业务
        return cartService.addCart(skuId, num, username);
    }

    //查看购物车
    @RequestMapping("/show")
    public List<OrderItemDto> getCart(){
        //从请求头中获取
        String username = TokenDecode.getUserInfo().get("username");
        List<TbOrderItemModel> carts = cartService.getCart(username);
        List<OrderItemDto> itemDtos=carts.stream().map(item->{
            OrderItemDto orderItemDto=new OrderItemDto();
            BeanUtils.copyProperties(item,orderItemDto );
            return orderItemDto;
        }).collect(Collectors.toList());
        return itemDtos;
    }

    //删除
    @RequestMapping("/delete")
    public void delete(String[] skuIds){
        //用户名，后面从token中获取
        //String username="张三";
        String username = TokenDecode.getUserInfo().get("username");
        //调用业务
        cartService.delete(username, skuIds);
    }

    //从redis查询选中购物车的商品集合
    @RequestMapping("/search")
    public List<OrderItemDto> search(@RequestParam("skuIds") String[] skuIds){
        //用户名，后面从token中获取
        //String username="张三";
        String username = TokenDecode.getUserInfo().get("username");
        //调用业务
        List<TbOrderItemModel> list = cartService.search(username, skuIds);
        List<OrderItemDto> itemDtos=list.stream().map(item->{
            OrderItemDto orderItemDto=new OrderItemDto();
            BeanUtils.copyProperties(item,orderItemDto );
            return orderItemDto;
        }).collect(Collectors.toList());
        return itemDtos;
    }



}
