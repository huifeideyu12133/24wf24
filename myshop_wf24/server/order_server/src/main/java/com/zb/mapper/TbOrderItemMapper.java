package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbOrderItemModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbOrderItemMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/02/19 11:25
 **/
@Mapper
public interface TbOrderItemMapper extends BaseMapper<TbOrderItemModel> {

		}
