package com.zb.controller;

import com.zb.config.TokenDecode;
import com.zb.entity.TbOrderModel;
import com.zb.service.TbOrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/Order")
public class OrderController {
    //注入业务
    @Resource
    TbOrderService tbOrderService;

    @PostMapping("/addOrder")
    public Integer addOrder(@RequestBody TbOrderModel order ,
                            @RequestParam("skuIds") String[] skuIds){

        //用户名，后面从token中获取
        //String username="张三";
        String username = TokenDecode.getUserInfo().get("username");
        //订单用户 用的登录用户
        order.setUsername(username);
        //调用业务
        Integer n=tbOrderService.addOrder(order,skuIds);
        return n;

    }
}
