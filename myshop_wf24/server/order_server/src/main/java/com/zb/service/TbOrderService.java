package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbOrderModel;


/**
 * @ClassName TbOrderService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/02/19 11:24
 **/
public interface TbOrderService extends IService<TbOrderModel> {

    Integer addOrder(TbOrderModel order, String[] skuIds);
}
