package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbTaskModel;

import java.util.List;


/**
 * @ClassName TbTaskService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/03/06 10:00
 **/
public interface TbTaskService extends IService<TbTaskModel> {
        public List<TbTaskModel> findBeforList();
        //加锁
        public Integer lock(Long id,Integer version);

}
