package com.zb.service;

import com.zb.entity.TbOrderItemModel;
import com.zb.entity.TbOrderModel;

import java.util.List;

//购物车
public interface CartService {
    //添加购物车
    public int addCart(String skuId,Integer num,String username);
    //购物车
    public List<TbOrderItemModel> getCart(String username);
    //删除购物车
    public void  delete(String username,String[] skuIds);
    //获取你选择的商品
    public List<TbOrderItemModel> search(String username,String[] skuIds);






}
