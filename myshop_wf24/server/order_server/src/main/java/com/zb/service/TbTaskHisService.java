package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbTaskHisModel;


/**
 * @ClassName TbTaskHisService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/03/06 10:00
 **/
public interface TbTaskHisService extends IService<TbTaskHisModel> {

        }
