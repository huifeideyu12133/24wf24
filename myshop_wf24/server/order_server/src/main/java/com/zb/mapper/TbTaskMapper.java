package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbTaskModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @ClassName TbTaskMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/03/06 10:00
 **/
@Mapper
public interface TbTaskMapper extends BaseMapper<TbTaskModel> {
    //获取一分钟前的任务集合
    @Select("SELECT * FROM tb_task WHERE TIMESTAMPDIFF( MINUTE,update_time, NOW())>1")
    public List<TbTaskModel> findBeforList();

    //加乐观锁
    public Integer lock(@Param("id") Long id,@Param("version") Integer version);

}
