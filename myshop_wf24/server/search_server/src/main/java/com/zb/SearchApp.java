package com.zb;


import com.zb.service.SearchService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@SpringBootApplication
@EnableDiscoveryClient
public class SearchApp {
    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext run = SpringApplication.run(SearchApp.class, args);
        SearchService bean = run.getBean(SearchService.class);
        //bean.createIndex();
        //导入数据
        //bean.importData();
        Map<String,Object> params=new HashMap<>();
        params.put("index", 1);
        params.put("size",50 );
        //分类和品牌的条件
        params.put("cateName", "手机");
        params.put("brandName","华为" );
        //规格参数
       /* params.put("spec_颜色","红色" );
        params.put("spec_版本","3GB+32GB" );*/
        //价格范围查找
        params.put("price","10000-*");

        //排序条件
        params.put("sortField", "price");
        params.put("sortRule","asc" );

        Map<String, Object> map = bean.search("手机", params);
        //获取结果数据

        List<String> cateList =(List<String>) map.get("categoryList");
        List<String> brandList =(List<String>) map.get("brandList");
        List<Map<String,Object>> result =(List<Map<String,Object>>) map.get("result");
        Map<String, Set> specMap=(Map<String, Set>)map.get("sepcMap");

        System.out.println("分类数据==============");
        //遍历 分类的数据
        for (String s : cateList) {
            System.out.println(s);
        }
        System.out.println("品牌数据==============");
        //遍历 品牌的数据
        for (String s : brandList) {
            System.out.println(s);
        }

        System.out.println("规格数据==============");
        for (Map.Entry<String, Set> enter : specMap.entrySet()) {
            System.out.println(
                    enter.getKey()+"\t"
                            +enter.getValue());
        }

        System.out.println("==============");
        //遍历 结果的数据
        for (Map<String, Object> m : result) {
            System.out.println(m);
        }




    }
}

