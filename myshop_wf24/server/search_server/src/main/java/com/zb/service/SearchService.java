package com.zb.service;


import java.util.Map;

//搜索服务的接口
public interface SearchService {
    //创建索引和映射
    public void createIndex() throws  Exception;
    //导入数据
    public void importData() throws  Exception;
    //查询
    public Map<String,Object> search(
            String keyword,
            Map<String,Object> params) throws  Exception;

}
