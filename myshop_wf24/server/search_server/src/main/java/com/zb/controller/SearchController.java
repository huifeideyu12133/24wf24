package com.zb.controller;

import com.zb.service.SearchService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

//张三代码修改
@RestController
public class SearchController {
    //注入业务
    @Resource
    private SearchService searchService;
    //查询
    @RequestMapping("/search")
    public Map<String, Object> search(
            @RequestParam Map<String, Object> params) throws Exception {
        return  searchService.search(
                params.get("keyword").toString(),
                params);
    }

}
