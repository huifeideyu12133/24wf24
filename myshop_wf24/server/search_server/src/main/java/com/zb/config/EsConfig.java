package com.zb.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//git修改代码
@Configuration
public class EsConfig {
//asdqwe
    //从配置文件获取es服务器ip地址和端口号
    @Value("${myes.host}")
    private String hostList;
    //创建客户端对象
    @Bean
    public RestHighLevelClient createClient(){
        String[] hosts = hostList.split(",");
        HttpHost[] httpHosts=new HttpHost[hosts.length];
        //遍历
        for (int i = 0; i < httpHosts.length; i++) {
            String item=hosts[i];
            httpHosts[i]=new HttpHost(item.split(":")[0],
                    Integer.parseInt(item.split(":")[1]));

        }
        return new RestHighLevelClient(RestClient.builder(httpHosts));
    }
}
