package com.zb.service.impl;

import com.alibaba.fastjson.JSON;
import com.zb.service.SearchService;
import org.apache.lucene.index.Term;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import javax.annotation.Resource;
import javax.swing.*;
import java.sql.*;
import java.util.*;

@Service
public class SearchServiceImpl implements SearchService {
    //注入客户端对象
    @Resource
    private RestHighLevelClient client;

    @Override
    public void createIndex() throws Exception {
        //请求对象
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("tb_sku");
        //索引的设置
        createIndexRequest.settings(
                Settings.builder()
                        .put("number_of_shards", 1)
                        .put("number_of_replicas", 0));
        //创建映射
        createIndexRequest.mapping("doc", "{\n" +
                "\t\"properties\": {\n" +
                "\t\t\"sn\": {\n" +
                "\t\t\t\"type\": \"text\"\n" +
                "\t\t},\n" +
                "\t\t\"name\": {\n" +
                "\t\t\t\"type\": \"text\",\n" +
                "\t\t\t\"analyzer\": \"ik_max_word\",\n" +
                "\t\t\t\"search_analyzer\": \"ik_smart\"\n" +
                "\t\t},\n" +
                "\t\t\"price\": {\n" +
                "\t\t\t\"type\": \"float\"\n" +
                "\t\t},\n" +
                "\t\t\"num\": {\n" +
                "\t\t\t\"type\": \"integer\"\n" +
                "\t\t},\n" +
                "\t\t\"alert_num\": {\n" +
                "\t\t\t\"type\": \"integer\"\n" +
                "\t\t},\n" +
                "\t\t\"image\": {\n" +
                "\t\t\t\"type\": \"text\"\n" +
                "\t\t},\n" +
                "\t\t\"images\": {\n" +
                "\t\t\t\"type\": \"text\"\n" +
                "\t\t},\n" +
                "\t\t\"weight\": {\n" +
                "\t\t\t\"type\": \"integer\"\n" +
                "\t\t},\n" +
                "\t\t\"create_time\": {\n" +
                "\t\t\t\"type\": \"date\",\n" +
                "\t\t\t\"format\": \"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd\"\n" +
                "\t\t},\n" +
                "\t\t\"update_time\": {\n" +
                "\t\t\t\"type\": \"date\",\n" +
                "\t\t\t\"format\": \"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd\"\n" +
                "\t\t},\n" +
                "\t\t\"spu_id\": {\n" +
                "\t\t\t\"type\": \"text\"\n" +
                "\t\t},\n" +
                "\t\t\"category_id\": {\n" +
                "\t\t\t\"type\": \"integer\"\n" +
                "\t\t},\n" +
                "\t\t\"category_name\": {\n" +
                "\t\t\t\"type\": \"keyword\"\n" +
                "\t\t},\n" +
                "\t\t\"brand_name\": {\n" +
                "\t\t\t\"type\": \"keyword\"\n" +
                "\t\t},\n" +
                "\t\t\"spec\": {\n" +
                "\t\t\t\"type\": \"keyword\"\n" +
                "\t\t},\n" +
                "\t\t\"sale_num\": {\n" +
                "\t\t\t\"type\": \"integer\"\n" +
                "\t\t},\n" +
                "\t\t\"comment_num\": {\n" +
                "\t\t\t\"type\": \"integer\"\n" +
                "\t\t},\n" +
                "\t\t\"status\": {\n" +
                "\t\t\t\"type\": \"keyword\"\n" +
                "\t\t},\n" +
                "\t\t\"version\": {\n" +
                "\t\t\t\"type\": \"integer\"\n" +
                "\t\t},\n" +
                "\t\t\"specMap\":{\n" +
                "               \"properties\":{\n" +
                "                  \"test\":{\n" +
                "                    \"type\": \"keyword\"\n" +
                "}\n" +
                "}\n" +
                "\t\t}\n" +
                "\t}\n" +
                "}\n", XContentType.JSON);

        //创建索引客户端
        IndicesClient indices = client.indices();
        //创建响应对象
        CreateIndexResponse createIndexResponse = indices
                .create(createIndexRequest, RequestOptions.DEFAULT);
        //得到响应结果
        boolean acknowledged = createIndexResponse.isAcknowledged();
        System.out.println(acknowledged);
    }

    @Override
    public void importData() throws Exception {
        //jdbc
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/shop_goods?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8",
                "root",
                "ok");
        String sql="SELECT * FROM tb_sku";
        PreparedStatement pstmt = con.prepareStatement(sql);
        //查询
        ResultSet rs = pstmt.executeQuery();
        //循环
        while(rs.next()){
            Map<String,Object> jsonMap=new HashMap<>();
            jsonMap.put("sn", rs.getString("sn"));
            jsonMap.put("name",rs.getString("name") );
            jsonMap.put("price",rs.getFloat("price") );
            jsonMap.put("num", rs.getInt("num"));
            jsonMap.put("alert_num", rs.getInt("alert_num"));
            jsonMap.put("image", rs.getString("image"));
            jsonMap.put("images", rs.getString("images"));
            jsonMap.put("weight", rs.getInt("weight"));
            jsonMap.put("spu_id", rs.getString("spu_id"));
            jsonMap.put("category_id", rs.getInt("category_id"));
            jsonMap.put("category_name", rs.getString("category_name"));
            jsonMap.put("brand_name", rs.getString("brand_name"));
            jsonMap.put("create_time", rs.getDate("create_time").toString());
            jsonMap.put("update_time", rs.getDate("update_time").toString());
            jsonMap.put("spec", rs.getString("spec"));
            jsonMap.put("sale_num", rs.getInt("sale_num"));
            jsonMap.put("comment_num", rs.getInt("comment_num"));
            jsonMap.put("status", rs.getString("status"));
            jsonMap.put("version", rs.getInt("version"));
            //规格数据
            Map<String,Object> specMap = JSON.parseObject(rs.getString("spec"), Map.class);
            jsonMap.put("specMap", specMap);
            //加到es
            //索引请求
            IndexRequest indexRequest =
                    new IndexRequest("tb_sku",
                            "doc", rs.getString("id"));
            //绑定数据
            indexRequest.source(jsonMap);
            //响应
            IndexResponse indexResponse =
                    client.index(indexRequest, RequestOptions.DEFAULT);
            System.out.println(
                    indexResponse.getResult());

        }
        //连接关闭
        con.close();


    }

    /**
     * es查询
     * @param keyword name 关键词
     * @param params 查询条件
     * @return map 既包含下面商品集合，也包含上面的规格信息
     * @throws Exception
     */
    @Override
    public Map<String, Object> search(String keyword, Map<String, Object> params) throws Exception {
        if(StringUtils.isEmpty(keyword)){
            keyword="华为";
        }
        //请求对象
        //请求对象
        SearchRequest searchRequest = new SearchRequest("tb_sku");
        searchRequest.types("doc");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //分页查询
        int from=(Integer.parseInt(
                params.get("index").toString())-1)*
                Integer.parseInt(params.get("size").toString());

        searchSourceBuilder.from(from);
        searchSourceBuilder.size(Integer.parseInt(params.get("size").toString()));

        //分组查询 分类分类 ；品牌分组，规格分组
        searchSourceBuilder.aggregation(AggregationBuilders.terms("cateGroup").field("category_name").size(50));
        searchSourceBuilder.aggregation(AggregationBuilders.terms("brandGroup").field("brand_name").size(50));
        searchSourceBuilder.aggregation(AggregationBuilders.terms("specGroup").field("spec").size(50));


        //多列查询
        MultiMatchQueryBuilder multiMatchQueryBuilder=
                QueryBuilders.multiMatchQuery(keyword,"name")
                .operator(Operator.OR);
        //布尔查询
        BoolQueryBuilder boolQueryBuilder=
                QueryBuilders.boolQuery();
        //条件
        boolQueryBuilder.must(multiMatchQueryBuilder);
        //过滤
        if(!StringUtils.isEmpty(params.get("cateName"))){
            TermsQueryBuilder termsQueryBuilder=
                    QueryBuilders.termsQuery("category_name",
                            params.get("cateName"));
            boolQueryBuilder.filter(termsQueryBuilder);

        }
        if(!StringUtils.isEmpty(params.get("brandName"))){
            TermsQueryBuilder termsQueryBuilder=
                    QueryBuilders.termsQuery("brand_name",
                            params.get("brandName"));
            boolQueryBuilder.filter(termsQueryBuilder);
        }






      /*  "specMap": {
            "颜色": "灰色",
             "尺码": "200度"
        }*/
      //遍历所有的规格参数
        for (String key : params.keySet()) {
            //判断规格参数是否存在，存在则过滤
            if(key.startsWith("spec_")){
                TermQueryBuilder termQueryBuilder=
                        QueryBuilders.termQuery(
                                "specMap."+key.substring(5)+".keyword"
                                ,params.get(key) );
                boolQueryBuilder.filter(termQueryBuilder);
            }
        }
        //价格范围
        if(!StringUtils.isEmpty(params.get("price"))){
            String[] prices = params.get("price").toString().split("-");
            RangeQueryBuilder rangeQueryBuilder ;
            if(prices[1].equals("*")) {
                 rangeQueryBuilder =
                        QueryBuilders.rangeQuery("price")
                                .gte(prices[0]);
            }else{
                 rangeQueryBuilder =
                        QueryBuilders.rangeQuery("price")
                                .gte(prices[0]).lte(prices[1]);
            }
            boolQueryBuilder.filter(rangeQueryBuilder);

        }
        //排序
        if(!StringUtils.isEmpty(params.get("sortField")) &&
                !StringUtils.isEmpty(params.get("sortRule"))){
            String rule=params.get("sortRule").toString();
            SortOrder order;
            if(rule.equals("asc")){
                order= SortOrder.ASC;
            }else{
                order=SortOrder.DESC;
            }
            searchSourceBuilder.sort(
                    new FieldSortBuilder(
                            params.get("sortField").toString()).order(order));
        }


        //绑定查询
        searchSourceBuilder.query(boolQueryBuilder);

        //创建高亮对象
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //设置标签
        highlightBuilder.preTags("<span style=’color:red;’>");
        highlightBuilder.postTags("</span>");
        //设置高亮字段
        highlightBuilder.fields().add(
                new HighlightBuilder.Field("name"));
        //將highlightBuilder对象添加到查询对象中
        searchSourceBuilder.highlighter(highlightBuilder);



        //查询哪些列
        searchSourceBuilder.fetchSource(new String[]{"name", "spu_id", "brand_name", "category_name","spec","specMap","price"}, new String[]{});
        searchRequest.source(searchSourceBuilder);
        //执行查询，得到响应结果
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        //结果数据
        SearchHits hitResult = searchResponse.getHits();
        SearchHit[] hits = hitResult.getHits();
        //源数据 集合
        List<Map<String,Object>> result=new ArrayList<>();
        //遍历
        for (SearchHit hit : hits) {
            String id = hit.getId();
            //System.out.println(id);
            //数据源
            Map<String, Object> map = hit.getSourceAsMap();
            //获取高亮数据
            Map<String, HighlightField> highlightFields =
                    hit.getHighlightFields();
            //获取name列对应的高亮值
            HighlightField nameFiled = highlightFields.get("name");
            if (nameFiled != null) {
                StringBuilder sb = new StringBuilder();
                Text[] nameText = nameFiled.getFragments();
                for (Text text : nameText) {
                    sb.append(text);
                }
                map.put("name", sb.toString());
            }

            result.add(map);
            //System.out.println(map);
        }


        //得到分组信息
        Terms cateGroup=searchResponse.getAggregations().get("cateGroup");
        Terms brandGroup=searchResponse.getAggregations().get("brandGroup");
        Terms specGroup=searchResponse.getAggregations().get("specGroup");

        //调用方法，获取分组的集合
        List<String> cateList = this.termsAsList(cateGroup);
        List<String> brandList = this.termsAsList(brandGroup);
        Map<String, Set> sepcMap = this.termsasMap(specGroup);


        Map<String, Object> resultData=new HashMap<>();
        resultData.put("categoryList",cateList);
        resultData.put("brandList",brandList );
        resultData.put("sepcMap", sepcMap);
        resultData.put("result", result);
        return resultData;
    }
    //规格数据的转化
    private Map<String, Set> termsasMap(Terms terms){
        Map<String,Set>  specMap=new HashMap<>();
        //调用方法，转成字符串集合
        List<String> list = this.termsAsList(terms);
        //遍历集合
        for (String str : list) {
            //{'颜色': 'mate20 翡冷翠', '版本': '6GB+64GB'}
            //一条json字符串转成一个map
            Map<String,String> map=JSON
                    .parseObject(str,Map.class);
            //遍历规格集合
            for (Map.Entry<String, String> enter : map.entrySet()) {
                //获取key和value
                String key = enter.getKey();//'颜色'
                String value = enter.getValue();//'mate20 翡冷翠'
                //在返回值集合中查看是否存在此key
                Set set = specMap.get(key);
                //判断当前的key是否是第一次加到specMap中
                if(set==null || set.size()==0){
                    set=new HashSet();
                }
                //set集合添加值
                set.add(value);
                //添加到返回的specMap中
                specMap.put(key, set);
            }
        }
        return specMap;
    }

    //分类 ，品牌数据的转化
    private List<String> termsAsList(Terms terms){
        List<String> list=new ArrayList<>();
        //遍历分组信息
        for (Terms.Bucket bucket : terms.getBuckets()) {
            //key
            String key = bucket.getKey().toString();
            list.add(key);
        }
        return list;
    }
}
