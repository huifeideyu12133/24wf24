package com.zb.service;

import java.util.Map;

//支付的业务接口
public interface PayService {

    /**
     * 调用微信的统一下单api，实现微信支付
     * @param tradeNo 订单号
     * @param total_fee 支付的钱
     * @return
     */
    public Map<String,String> createOrderNavive(String tradeNo,Integer total_fee) throws  Exception;

    Map<String, String> search(String out_trade_no) throws Exception;
}
