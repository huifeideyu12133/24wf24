package com.zb.controller;

import com.alibaba.fastjson.JSON;
import com.github.wxpay.sdk.WXPayUtil;
import com.zb.config.MQConfig;
import com.zb.service.PayService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {
    @Resource
    private PayService payService;

    //mq对象
    @Resource
    private RabbitTemplate rabbitTemplate;




    @RequestMapping("/create")
    public Map<String, String> createOrderNavive(
            String tradeNo, Integer total_fee) throws Exception {
        System.out.println("统一下单");
        return payService.createOrderNavive(tradeNo,total_fee );
    }
    //微信服务器 获取用户支付后，通知我商户平台
    @RequestMapping("/mynotifyurl")
    public String notifyurl(HttpServletRequest request){
        System.out.println("微信服务器的回调方法");
        try {
            //获取的输入流
            InputStream is = request.getInputStream();
            //写对象
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            //写到输出流
            byte[] data=new byte[1024];
            int len=0;
            while((len=is.read(data))!=-1){
                baos.write(data,0,len);
            }
            baos.close();
            is.close();
            //微信发来的xml
            String xml=new String(baos.toByteArray(),"utf-8");
            System.out.println(xml);
            //转成map
            Map<String, String> map = WXPayUtil.xmlToMap(xml);
            //转成json字符串
            String json = JSON.toJSONString(map);

            //不解耦解决方案修改订单支付的状态，openfeign 远程调用订单服务
            //mq的解决方案
            rabbitTemplate.convertAndSend(
                    MQConfig.payExchange,
                    MQConfig.payRouting ,
                    json);

            //商品处理后返回给微信参数，这样微信就不会一直发了
            Map<String,String> mapReturn=new HashMap<>();
            mapReturn.put("return_code", "success");
            mapReturn.put("return_msg", "ok");
            return WXPayUtil.mapToXml(mapReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    //查询订单
    @RequestMapping("/search")
    public  Map<String,String> searchOrder(String out_trade_no)
            throws  Exception{
        return payService.search(out_trade_no);
    }
}
