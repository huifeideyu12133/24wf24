package com.zb.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//mq的配置类
@Configuration
public class MQConfig {
    public static final String payExchange="payExchange";
    public static final String payQueue="payQueue";
    public static final String payRouting="payRouting";

    //创建交换机
    @Bean(payExchange)
    public Exchange createPayExchange(){
        return ExchangeBuilder
                .topicExchange(payExchange)
                .durable(true).build();
    }
    //创建队列
    @Bean(payQueue)
    public Queue createPayQueue(){
        //队列名，持久化，独占连接，自动删除，队列属性
        return new Queue(payQueue,
                true,
                false,
                false,
                null);
    }
    //交换机和队列绑定
    @Bean
    public Binding bindingPayExchangeQueue(
            @Qualifier(payExchange) Exchange exchange,
            @Qualifier(payQueue) Queue queue){
        return BindingBuilder.bind(queue)
                .to(exchange)
                .with(payRouting).noargs();

    }

}
