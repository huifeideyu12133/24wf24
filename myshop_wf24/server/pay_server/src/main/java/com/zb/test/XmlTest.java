package com.zb.test;

import com.github.wxpay.sdk.WXPayUtil;

import java.util.HashMap;
import java.util.Map;

public class XmlTest {
    public static void main(String[] args) throws  Exception{
        //随机数
        String str = WXPayUtil.generateNonceStr();
        System.out.println("随机数===:"+str);

        //map转成xml
        Map<String,String> param=new HashMap<>();
        param.put("username", "zs");
        param.put("password", "123");
        String xml = WXPayUtil.generateSignedXml(param, str);
        System.out.println(xml);
        System.out.println("=================");
        //xml转成map
        Map<String, String> map = WXPayUtil.xmlToMap(xml);
        System.out.println(map);


    }
}
