package com.zb.service.impl;

import com.github.wxpay.sdk.WXPayUtil;
import com.zb.service.PayService;
import com.zb.util.HttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PayServiceImpl implements PayService {
    //获取配置文件的信息
    @Value("${weixin.appid}")
    private String appid;//商户id
    @Value("${weixin.partner}")
    private String partner;
    @Value("${weixin.partnerkey}")
    private String  partnerkey;
    @Value("${weixin.notifyurl}")
    private String notifyurl;





    @Override
    public Map<String, String> createOrderNavive(String tradeNo, Integer total_fee) throws  Exception{
        Map<String, String> param=new HashMap<>();
        param.put("appid", appid);
        param.put("mch_id", partner);
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        param.put("body","我的应用程序");
        param.put("out_trade_no",tradeNo );
        param.put("total_fee",total_fee.toString());
        param.put("spbill_create_ip","127.0.0.1" );
        param.put("notify_url", notifyurl);//回调地址
        param.put("trade_type","NATIVE");
        //map转成xml
        String xml =
                WXPayUtil.generateSignedXml(param,
                        partnerkey);
        //调用
        HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
        httpClient.setHttps(true);
        httpClient.setXmlParam(xml);
        httpClient.post();//发送
        //得到返回结果
        String xmlContent = httpClient.getContent();
        //xml转成map
        Map<String, String> content =
                WXPayUtil.xmlToMap(xmlContent);
        System.out.println("==="+content);
        return content;
    }

    //查询订单
    @Override
    public Map<String, String> search(String out_trade_no) throws Exception {
        Map<String, String> param=new HashMap<>();
        param.put("appid", appid);
        param.put("mch_id", partner);
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        param.put("out_trade_no",out_trade_no );
        //map转成xml
        String xml =
                WXPayUtil.generateSignedXml(param,
                        partnerkey);
        //调用
        HttpClient httpClient=new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
        httpClient.setHttps(true);
        httpClient.setXmlParam(xml);
        httpClient.post();//发送
        //得到返回结果
        String xmlContent = httpClient.getContent();
        //xml转成map
        Map<String, String> content =
                WXPayUtil.xmlToMap(xmlContent);
        System.out.println("==="+content);
        return content;
    }
}
