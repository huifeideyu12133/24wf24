package com.zb;

import com.zb.util.CanalUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class ConentApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ConentApp.class, args);
        //项目启动，调用工具类test方法
        CanalUtil bean = run.getBean(CanalUtil.class);
        bean.test();
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
