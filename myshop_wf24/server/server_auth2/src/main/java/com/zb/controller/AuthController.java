package com.zb.controller;

import com.zb.oauth.util.AuthToken;
import com.zb.service.AuthService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Resource
    AuthService authService;
    //从配置文件获取认证的用户名和密码
    @Value("${secret.clientId}")
    private String clientId;
    @Value("${secret.clientSecret}")
    private String clientSecret;

    @RequestMapping("/getToken")
    public AuthToken getToken(String username,
                              String password){
        return authService.getToken(username, password, clientId, clientSecret);
    }
}
