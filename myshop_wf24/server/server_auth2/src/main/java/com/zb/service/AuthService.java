package com.zb.service;


import com.zb.oauth.util.AuthToken;

//认证框架 业务
public interface AuthService {
    //远程调用认证服务，生成令牌
    public AuthToken getToken(String username,
                              String password,
                              String clientId,
                              String clientSecret);
}
