package com.zb.service.impl;

import com.zb.oauth.util.AuthToken;
import com.zb.service.AuthService;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {

    @Resource
    RestTemplate restTemplate;
    @Resource
    DiscoveryClient discoveryClient;

    @Override
    public AuthToken getToken(String username, String password, String clientId, String clientSecret) {
        //从注册中心，获取认证服务ip地址和端口号
        List<ServiceInstance> instances = discoveryClient.getInstances("user-auth");
        ServiceInstance serviceInstance = instances.get(0);
        //地址
        String url="http://"+serviceInstance.getHost()+":"+serviceInstance.getPort()+"/oauth/token";
        System.out.println("url:"+url);

        //参数1 密码模式，用户名，密码
        LinkedMultiValueMap<String,String> map=new LinkedMultiValueMap<>();
        map.add("grant_type", "password");
        map.add("username", username);
        map.add("password", password);
        //参数2 加密的请求头，根据认证用户名和密码获取
        LinkedMultiValueMap<String,String> header=new LinkedMultiValueMap<>();
        header.add("Authorization",
                this.httpbasic(clientId,clientSecret ));

        //restTemplate远程调用
        ResponseEntity<Map> exchange = restTemplate.exchange(url,
                HttpMethod.POST,
                new HttpEntity<>(map, header),
                Map.class);
        //从返回结果获取令牌对象
        //获取map结果
        Map exchangeBody = exchange.getBody();
        //封装成AuthToken对象
        AuthToken authToken=new AuthToken();
        //长令牌
        authToken.setAccessToken(exchangeBody.get("access_token").toString());
        //刷新令牌
        authToken.setRefreshToken(exchangeBody.get("refresh_token").toString());
        //短令牌
        authToken.setJti(exchangeBody.get("jti").toString());

        return authToken;
    }

    private   String httpbasic(String clientId,
                               String clientSecret){
        String str=clientId+":"+clientSecret;
        byte[] encode = Base64Utils.encode(str.getBytes());
        return "Basic "+new String(encode);

    }
}
