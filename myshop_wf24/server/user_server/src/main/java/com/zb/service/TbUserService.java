package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbUserModel;


/**
 * @ClassName TbUserService
 * @Description 用户表服务接口
 * @Author xm
 * @Date 2024/01/11 12:11
 **/
public interface TbUserService extends IService<TbUserModel> {

}
