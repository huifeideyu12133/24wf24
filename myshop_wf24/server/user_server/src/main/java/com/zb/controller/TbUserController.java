package com.zb.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zb.dto.UserDto;
import com.zb.entity.TbUserModel;
import com.zb.service.TbUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName TbUserController
 * @Description 用户表控制器
 * @Author xm
 * @Date 2024/01/11 12:11
 **/
@RestController
@RequestMapping("/user")
@Api(value = "TbUserController", tags = {"用户表控制器"})
public class TbUserController {

    @Autowired
    public TbUserService tbUserService;

    //根据用户名获取用户传输对象
    @RequestMapping("/getUser/{username}")
    public UserDto getUser(@PathVariable("username") String username){
        LambdaQueryWrapper<TbUserModel> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(TbUserModel::getUsername,username );
        //唯一查
        TbUserModel one=tbUserService.getOne(queryWrapper);
        //System.out.println("用户:"+one);
        //返回对象
        UserDto userDto=new UserDto();
        BeanUtils.copyProperties(one, userDto);
        System.out.println("用户:"+userDto);
        return  userDto;

    }


    //获取所有用户
    @RequestMapping("/all")
    public List<UserDto> getUserList(){
        List<TbUserModel> list = tbUserService.list();
        //java8 stream流 方式复制list
        List<UserDto> dtoList = list.stream().map(u -> {
            UserDto userDto = new UserDto();
            //复制属性
            BeanUtils.copyProperties(u, userDto);
            return userDto;
        }).collect(Collectors.toList());
        return dtoList;
    }


}
