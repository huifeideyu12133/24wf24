package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbUserModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbUserMapper
 * @Description 用户表mapper接口
 * @Author xm
 * @Date 2024/01/11 12:11
 **/
@Mapper
public interface TbUserMapper extends BaseMapper<TbUserModel> {

}
