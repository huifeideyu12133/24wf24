package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbUserModel;
import com.zb.mapper.TbUserMapper;
import com.zb.service.TbUserService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbUserServiceImpl
 * @Description 用户表服务接口实现
 * @Author xm
 * @Date 2024/01/11 12:11
 **/
@Service
public class TbUserServiceImpl extends ServiceImpl<TbUserMapper, TbUserModel> implements TbUserService {

}
