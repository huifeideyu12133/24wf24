package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbCategoryModel;


/**
 * @ClassName TbCategoryService
 * @Description 商品类目服务接口
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
public interface TbCategoryService extends IService<TbCategoryModel> {

        }
