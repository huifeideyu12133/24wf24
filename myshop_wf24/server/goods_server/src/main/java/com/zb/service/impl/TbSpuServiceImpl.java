package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbSpuModel;
import com.zb.mapper.TbSpuMapper;
import com.zb.service.TbSpuService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbSpuServiceImpl
 * @Description 服务接口实现
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Service
public class TbSpuServiceImpl extends ServiceImpl<TbSpuMapper, TbSpuModel> implements TbSpuService {

}
