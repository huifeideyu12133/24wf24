package com.zb.controller;

import com.zb.dto.BrandDto;
import com.zb.dto.CategoryDto;
import com.zb.dto.SkuDto;
import com.zb.dto.SpuDto;
import com.zb.entity.TbBrandModel;
import com.zb.entity.TbCategoryModel;
import com.zb.entity.TbSkuModel;
import com.zb.entity.TbSpuModel;
import com.zb.service.TbBrandService;
import com.zb.service.TbCategoryService;
import com.zb.service.TbSkuService;
import com.zb.service.TbSpuService;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/tb-sku")
public class SkuController {
    @Resource
    private TbSkuService tbSkuService;
    @Resource
    private TbSpuService tbSpuService;
    @Resource
    private TbBrandService tbBrandService;
    @Resource
    private TbCategoryService tbCategoryService;

    //根据skuId获取商品详情的数据信息
    @GetMapping("/info/{skuId}")
    public Map<String,Object> info(@PathVariable("skuId") String skuId){
        Map<String,Object> map=new HashMap<>();
        TbSkuModel skuModel = tbSkuService.getById(skuId);
        TbSpuModel spuModel = tbSpuService.getById(skuModel.getSpuId());
        //根据spu的三个分类id得到三级分类对象
        TbCategoryModel c1 = tbCategoryService.getById(spuModel.getCategory1Id());
        TbCategoryModel c2 = tbCategoryService.getById(spuModel.getCategory2Id());
        TbCategoryModel c3 = tbCategoryService.getById(spuModel.getCategory3Id());
        TbBrandModel brandModel = tbBrandService.getById(spuModel.getBrandId());
        //把model实体换成dto实体
        SkuDto skuDto=new SkuDto();
        SpuDto spuDto=new SpuDto();
        CategoryDto cd1=new CategoryDto();
        CategoryDto cd2=new CategoryDto();
        CategoryDto cd3=new CategoryDto();
        BrandDto brandDto=new BrandDto();
        BeanUtils.copyProperties(skuModel, skuDto);
        BeanUtils.copyProperties(spuModel, spuDto);
        BeanUtils.copyProperties(c1, cd1);
        BeanUtils.copyProperties(c2, cd2);
        BeanUtils.copyProperties(c3, cd3);
        BeanUtils.copyProperties(brandModel, brandDto);
        //加到map
        map.put("sku", skuDto);
        map.put("spu",spuDto );
        map.put("c1",cd1 );
        map.put("c2",cd2 );
        map.put("c3",cd3 );
        map.put("brand",brandDto );
        return map;
    }

    //更改商品库存
    @RequestMapping("/updateNum")
    public String updateNum(@RequestParam("id") String id,
                            @RequestParam("num") Integer num){
        //int a=10/0;//运行错误
        //根据skuid获取商品对象
        TbSkuModel skuModel = tbSkuService.getById(id);
        skuModel.setNum(skuModel.getNum()-num);
        //更新
        tbSkuService.updateById(skuModel);
        return "success";

    }
}
