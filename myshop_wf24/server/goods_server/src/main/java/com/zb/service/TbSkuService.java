package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbSkuModel;


/**
 * @ClassName TbSkuService
 * @Description 商品表服务接口
 * @Author xm
 * @Date 2024/02/16 10:43
 **/
public interface TbSkuService extends IService<TbSkuModel> {

        }
