package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbSpuModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbSpuMapper
 * @Description mapper接口
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Mapper
public interface TbSpuMapper extends BaseMapper<TbSpuModel> {

		}
