package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbCategoryModel;
import com.zb.mapper.TbCategoryMapper;
import com.zb.service.TbCategoryService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbCategoryServiceImpl
 * @Description 商品类目服务接口实现
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Service
		public class TbCategoryServiceImpl extends ServiceImpl<TbCategoryMapper, TbCategoryModel> implements TbCategoryService {

		}
