package com.zb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @ClassName TbCategoryModel
 * @Description 商品类目模型对象
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Data
    @EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_category")
@ApiModel(value = "TbCategoryModel", description = "商品类目")
public class TbCategoryModel implements Serializable{

private static final long serialVersionUID=1L;

        @ApiModelProperty(value = "分类ID")
                    @TableId(value = "id", type = IdType.AUTO)
                private Integer id;

        @ApiModelProperty(value = "分类名称")
        private String name;

        @ApiModelProperty(value = "商品数量")
        private Integer goodsNum;

        @ApiModelProperty(value = "是否显示")
        private String isShow;

        @ApiModelProperty(value = "是否导航")
        private String isMenu;

        @ApiModelProperty(value = "排序")
        private Integer seq;

        @ApiModelProperty(value = "上级ID")
        private Integer parentId;

        @ApiModelProperty(value = "模板ID")
        private Integer templateId;


        }
