package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbBrandModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbBrandMapper
 * @Description 品牌表mapper接口
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Mapper
public interface TbBrandMapper extends BaseMapper<TbBrandModel> {

		}
