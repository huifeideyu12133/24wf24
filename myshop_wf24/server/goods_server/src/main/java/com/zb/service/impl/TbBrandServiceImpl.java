package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbBrandModel;
import com.zb.mapper.TbBrandMapper;
import com.zb.service.TbBrandService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbBrandServiceImpl
 * @Description 品牌表服务接口实现
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Service
		public class TbBrandServiceImpl extends ServiceImpl<TbBrandMapper, TbBrandModel> implements TbBrandService {

		}
