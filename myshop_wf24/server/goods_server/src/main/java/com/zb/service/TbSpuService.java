package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbSpuModel;


/**
 * @ClassName TbSpuService
 * @Description 服务接口
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
public interface TbSpuService extends IService<TbSpuModel> {

        }
