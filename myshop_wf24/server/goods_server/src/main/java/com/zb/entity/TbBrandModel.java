package com.zb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @ClassName TbBrandModel
 * @Description 品牌表模型对象
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Data
    @EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_brand")
@ApiModel(value = "TbBrandModel", description = "品牌表")
public class TbBrandModel implements Serializable{

private static final long serialVersionUID=1L;

        @ApiModelProperty(value = "品牌id")
                    @TableId(value = "id", type = IdType.AUTO)
                private Integer id;

        @ApiModelProperty(value = "品牌名称")
        private String name;

        @ApiModelProperty(value = "品牌图片地址")
        private String image;

        @ApiModelProperty(value = "品牌的首字母")
        private String letter;

        @ApiModelProperty(value = "排序")
        private Integer seq;


        }
