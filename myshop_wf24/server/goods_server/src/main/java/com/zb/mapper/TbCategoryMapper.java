package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbCategoryModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbCategoryMapper
 * @Description 商品类目mapper接口
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
@Mapper
public interface TbCategoryMapper extends BaseMapper<TbCategoryModel> {

		}
