package com.zb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zb.entity.TbSkuModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName TbSkuMapper
 * @Description 商品表mapper接口
 * @Author xm
 * @Date 2024/02/16 10:43
 **/
@Mapper
public interface TbSkuMapper extends BaseMapper<TbSkuModel> {

		}
