package com.zb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zb.entity.TbSkuModel;
import com.zb.mapper.TbSkuMapper;
import com.zb.service.TbSkuService;
import org.springframework.stereotype.Service;

/**
 * @ClassName TbSkuServiceImpl
 * @Description 商品表服务接口实现
 * @Author xm
 * @Date 2024/02/16 10:43
 **/
@Service
		public class TbSkuServiceImpl extends ServiceImpl<TbSkuMapper, TbSkuModel> implements TbSkuService {

		}
