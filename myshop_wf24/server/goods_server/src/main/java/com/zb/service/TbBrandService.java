package com.zb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zb.entity.TbBrandModel;


/**
 * @ClassName TbBrandService
 * @Description 品牌表服务接口
 * @Author xm
 * @Date 2024/02/16 10:44
 **/
public interface TbBrandService extends IService<TbBrandModel> {

        }
