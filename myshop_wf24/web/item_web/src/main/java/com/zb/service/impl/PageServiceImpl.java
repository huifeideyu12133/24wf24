package com.zb.service.impl;

import com.alibaba.fastjson.JSON;
import com.zb.client.SkuFeignClient;
import com.zb.service.PageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import java.io.File;
import java.io.PrintWriter;
import java.util.Map;

@Service
public class PageServiceImpl implements PageService {
    @Resource
    private SkuFeignClient skuFeignClient;

    @Resource
    private TemplateEngine templateEngine;

    //页面路径
    @Value("${pagepath}")
    private String pagepath;

    @Override
    public Map<String, Object> buildData(String skuId) throws Exception {
        //远程调用
        Map<String, Object> map = skuFeignClient.info(skuId);
        //处理小图片的数据，把小图片地址拿出，放到map中
        Map<String,Object> spu=(Map<String,Object>)map.get("spu");
        String[] images = spu.get("images").toString().split(",");
        map.put("images",images);
        //处理规格数据
        Map specItems = JSON.parseObject(spu.get("specItems").toString(), Map.class);
        map.put("specItems", specItems);

        return map;
    }

    @Override
    public void createPage(String skuId) throws Exception {
        //获取数据
        Map<String, Object> data = this.buildData(skuId);
        Context context=new Context();
        //存数据
        context.setVariables(data);
        //检查文件是否存在
        File baseFile=new File(pagepath);
        if(!baseFile.exists()){
            //不存在，创建文件夹
            baseFile.mkdir();
        }
        //文件对象 第一个参数地址，第二个参数，文件名
        File itemFile=new File(baseFile,skuId+".html");
        PrintWriter printWriter=new PrintWriter(itemFile,"utf-8");
        //模板引擎的写
        templateEngine.process("item", context,printWriter);
    }
}
