package com.zb.service;


import java.util.Map;

public interface PageService {
    //绑定数据
    public Map<String,Object> buildData(String skuId) throws  Exception;
    //生成页面
    public void createPage(String skuId) throws  Exception;
}
