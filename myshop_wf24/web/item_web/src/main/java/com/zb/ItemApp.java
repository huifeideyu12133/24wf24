package com.zb;


import com.zb.service.PageService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class ItemApp {
    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext run = SpringApplication.run(ItemApp.class, args);
        //业务对象
        PageService pageService = run.getBean(PageService.class, args);
        pageService.createPage("100000004580");

    }
}
