package com.zb.client;

import com.zb.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "user-server")
public interface UserFeign {
    //根据用户名获取用户传输对象
    @RequestMapping("/user/getUser/{username}")
    public UserDto getUser(@PathVariable("username") String username);


}
