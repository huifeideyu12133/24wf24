package com.zb.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(value = "goods-server")
public interface SkuFeignClient {
    @GetMapping("/tb-sku/info/{skuId}")
    public Map<String,Object> info(@PathVariable("skuId") String skuId);

    @RequestMapping("/tb-sku/updateNum")
    public String updateNum(@RequestParam("id") String id,
                            @RequestParam("num") Integer num);
}
