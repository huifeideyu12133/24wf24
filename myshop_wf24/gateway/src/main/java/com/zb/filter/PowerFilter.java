package com.zb.filter;

import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Component
public class PowerFilter implements GlobalFilter, Ordered {

    @Resource
    RedisTemplate redisTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取请求对象
        ServerHttpRequest request = exchange.getRequest();
        //获取响应对象
        ServerHttpResponse response = exchange.getResponse();
        //获取地址，用户服务的根据用户名获取用户对象，放行
        String path=request.getURI().getPath().toString();
        if(path.startsWith("/api/user/getUser")){
            return  chain.filter(exchange);
        }
        //从请求头获取短令牌
        String jti = request.getHeaders().getFirst("jti");
        if(StringUtils.isEmpty(jti)){
            //从参数取
            jti=request.getQueryParams().getFirst("jti");
            if(StringUtils.isEmpty(jti)){
                //拒绝访问响应
                response.setStatusCode(HttpStatus.NOT_ACCEPTABLE);
                return response.setComplete();
            }

        }
        //从redis中根据短令牌获取长令牌
        String accessToken = redisTemplate.boundHashOps("token")
                .get(jti).toString();
        //封装访问资源服务器的请求头
        request.mutate().header("Authorization",
                "bearer "+accessToken);
        //往下路由

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
