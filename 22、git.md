# 分布式版本控制系统

## Git与SVN区别

Git是分布式版本控制系统，SVN是集中式版本控制系统

**SVN优点：**

版本库是集中放在中央服务器的，干活的时候用的自己的电脑，从中央服务器获取最新版本，然后工作，工作完成后，将活推送的中央服务器

**SVN缺点：**

必须联网才能工作，局域网下带宽大速度块，但是在互联网下网速慢影响效率

**GIT优点：**

分布式开发、强调个体

公共服务压力和数据量都不会太大

速度快、灵活

**GIT缺点：**

资料少（中文资料）

学习周期相对较长

不符合常规思维

代码保密性差，一旦开发者把整个库克隆下来就可以完全公开所有代码和版本

### GIT工作区、版本区和暂存区区别

**工作区(Working Directory)：**就是你电脑本地硬盘目录

**版本库(Repository)：**工作区有个隐藏目录.git，它就是Git的本地版本库

**暂存区(stage)：**一般存放在”git目录”下的index文件（.git/index）中，所以我们把暂存区有时也叫作索引（index） 

![img](Untitled.assets/wps1.jpg)

Git自动创建的第一个分支--master，以及指向master的一个指针叫HEAD

**文件往Git版本库添加的时候分两步**

1、git  add----把文件纳入Git管理，实际就是把本地文件修改添加到暂存区

2、git commit 提交更改，实际是把暂存区的所有内容提交到当前分支

## 使用

设置全局用户名

~~~
git config --global user.name 'zhangsan' 
~~~

配置全局邮箱

~~~
git config --global user.email 'zhangsan@126.com'
~~~

（用户名和邮箱可以每个项目配置一个）

在用户中查看设置

![1710685926746](Untitled.assets/1710685926746.png)

### 命令

~~~
git init  //初始化   创建版本库
~~~

![1710686251497](Untitled.assets/1710686251497.png)

#### 查看文件状态 提交修改

##### 查看文件状态

~~~
git status // 查看当前路径下所有文件的状态 
git status a.txt // 查看当前路径下a.txt文件的状态
~~~

![1710686314303](Untitled.assets/1710686314303.png)

红色表示未被git管理的文件（绿色表示已加入暂存区）

##### 加入到暂存区

~~~
git add a.txt // 将a.txt加入到暂存区
~~~

##### 提交

~~~
git commit -m 'first commit' a.txt //-m'aaa'是提交注释，必须添加否则提交不了
~~~

#### 日志

##### 查看

~~~
git log //查看当前路径下的所有文件的版本日志信息
git log a.txt //查看当前路径下的a.txt的版本日志信息
~~~

![1710686855287](Untitled.assets/1710686855287.png)

每次提交后，版本号都不相同

##### 版本号信息

~~~
git reflog a.txt //只显示前7位
~~~

##### 版本的回退与穿梭

~~~
git reset --hard HEAD^                  // 回退上一个版本
git reset --hard HEAD~40                // 回退40个版本以前
git reset --hard 版本号(如上面的fbf510a)  // 穿梭到fbf510a那个版本
~~~

##### 对比文件差异（不同）

~~~
git diff a.txt
~~~

![img](Untitled.assets/wps2.jpg)

修改a.txt文件后，没有通过add存到暂存区的对比方法，然后使用diff对比工作区和本地版本库

#### 撤销文件修改

##### 撤销未加入暂存区的文件修改

~~~
git checkout -- a.txt//注意 -- 前后都有空格
~~~

##### 撤销以加入暂存区但为提交的文件修改

~~~
git reset HEAD a.txt // 先回到之前版本库a.txt的代码 
git checkout -- a.txt // 再checkout a.txt
~~~

##### 撤销已加入暂存区以提交的文件修改

~~~
git reset –hard HEAD^ // 整体回退到之前版本
~~~

#### 删除文件

~~~
git rm -f a.txt // 删除a.txt 
git commit -m 'delete a.txt' a.txt // 提交操作
~~~

删除后，使用git status命令，可以看到a.txt文件为绿色，需要提交才能生效

#### 分支操作

##### 创建分支

~~~
git branch aa // 创建(aa)分支
~~~

##### 查看分支

~~~
git branch
~~~

##### 切换分支

~~~
git checkout aa //切换到aa分支
~~~

#### 分支合并

##### 无冲突

先在子分支上进行修改、提交

~~~
git commit -m 'aa update file' a.txt
~~~

然后在主分支上进行合并

~~~
git merge aa
~~~

更新完成

##### 有冲突

当两个子分支都对一个文件进行修改，并被提交会出现冲突

这时会展示出冲突位置

![img](Untitled.assets/wps3.jpg)

需要主分支对文件冲突部分进行手动修改确认

解决冲突点后，在进行git add a.txt提交

##### 删除分支

~~~
git branch -d aa
~~~



~~~http
https://gitee.com/
huifeideyu12133
huifeiyu1@
~~~



